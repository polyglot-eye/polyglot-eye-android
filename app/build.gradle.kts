import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties
import com.android.build.gradle.internal.tasks.factory.dependsOn

@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.kapt)
    alias(libs.plugins.hilt)
    alias(libs.plugins.ktlint)
    alias(libs.plugins.serialization)
    alias(libs.plugins.google.services)
    alias(libs.plugins.firebase.crashlytics)
}

val keyAliasValue: String = gradleLocalProperties(rootDir).getProperty("keyAlias")
val keyPasswordValue: String = gradleLocalProperties(rootDir).getProperty("keyPassword")
val keyStoreLocationValue: String = gradleLocalProperties(rootDir).getProperty("storeFile")
val keyStorePasswordValue: String = gradleLocalProperties(rootDir).getProperty("storePassword")
val applicationIdValue: String by rootProject.extra
val versionNameValue: String by rootProject.extra
val versionMinSdkValue: Int by rootProject.extra
val versionTargetSdkValue: Int by rootProject.extra
val versionCompileSdkValue: Int by rootProject.extra
val jvmTargetValue: String by rootProject.extra

android {
    namespace = applicationIdValue
    compileSdk = versionCompileSdkValue

    defaultConfig {
        applicationId = applicationId
        minSdk = versionMinSdkValue
        targetSdk = versionTargetSdkValue
        versionCode = 1
        versionName = versionNameValue

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    signingConfigs {
        create("release") {
            keyAlias = keyAliasValue
            keyPassword = keyPasswordValue
            storeFile = file(keyStoreLocationValue)
            storePassword = keyStorePasswordValue
        }
    }

    buildTypes {
        debug {
            applicationIdSuffix = ".debug"
            resValue("string", "app_name", "Polyglot Eye - Debug")
            isMinifyEnabled = false
            isDebuggable = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
        create("staging") {
            applicationIdSuffix = ".staging"
            resValue("string", "app_name", "Polyglot Eye - Staging")
            isMinifyEnabled = false
            isDebuggable = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
            signingConfig = signingConfigs.getByName("release")
        }
        release {
            resValue("string", "app_name", "Polyglot Eye")
            isMinifyEnabled = false
            isDebuggable = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
            signingConfig = signingConfigs.getByName("release")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = jvmTarget
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = libs.versions.kotlinCompilerExtensionVersion.get()
    }
    packaging {
        resources {
            excludes += "/META-INF/*"
        }
    }
    androidResources {
        noCompress += "tflite"
    }

    project.tasks.preBuild.dependsOn("ktlintFormat")
}

dependencies {

    kapt(libs.hilt.compiler)

    implementation(platform(libs.compose.bom))
    implementation(platform(libs.google.cloud.bom))
    implementation(platform(libs.firebase.bom))

    implementation(libs.core.ktx)
    implementation(libs.bundles.lifecycle)
    implementation(libs.bundles.compose)
    implementation(libs.hilt.android)
    implementation(libs.bundles.camera)
    implementation(libs.bundles.tensorflow)
    implementation(libs.accoompanist.permissions)
    implementation(libs.bundles.google.cloud)
    implementation(libs.bundles.datastore)
    implementation(libs.kotlinx.serialization.json)
    implementation(libs.crypto.tink)
    implementation(libs.bundles.firebase)
    implementation(libs.grpc.okhttp)
    implementation(libs.bundles.unitTests)

    debugImplementation(libs.androidx.compose.ui.tooling)
    debugImplementation(libs.androidx.compose.ui.test.manifest)

    testImplementation(libs.bundles.unitTests)

    androidTestImplementation(platform(libs.compose.bom))
    androidTestImplementation(libs.bundles.instrumentedTests)
}
