@file:OptIn(ExperimentalCoroutinesApi::class)

package com.filippetrovic.polygloteye.data.datasource

import com.filippetrovic.polygloteye.data.datasource.translation.GoogleTranslationDataSource
import com.filippetrovic.polygloteye.data.datasource.translation.model.LanguageModel
import com.google.cloud.translate.Language
import com.google.cloud.translate.Translate
import com.google.cloud.translate.Translation
import io.mockk.Called
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test

class GoogleTranslationDataSourceTests {

    @MockK
    private lateinit var translationService: Translate

    private lateinit var sut: GoogleTranslationDataSource

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        sut = GoogleTranslationDataSource(UnconfinedTestDispatcher(), translationService)
    }

    @Test
    fun `test getSupportedLanguages, exception occurred, error returned`() = runTest {
        // Given
        every { translationService.listSupportedLanguages() } throws Exception()

        // When
        val response = sut.getSupportedLanguages()

        // Then
        verify { translationService.listSupportedLanguages() }
        assertTrue(response.isError)
    }

    @Test
    fun `test getSupportedLanguages, two different languages, languages list returned`() = runTest {
        // Given
        val expectedResult = listOf(
            LanguageModel("en", "English"),
            LanguageModel("sr", "Serbian")
        )
        val language1: Language = mockk()
        val language2: Language = mockk()

        every { translationService.listSupportedLanguages() } returns listOf(language1, language2)
        every { language1.code } returns "en"
        every { language1.name } returns "English"
        every { language2.code } returns "sr"
        every { language2.name } returns "Serbian"

        // When
        val response = sut.getSupportedLanguages()

        // Then
        verify { translationService.listSupportedLanguages() }
        assertEquals(expectedResult, response.result())
    }

    @Test
    fun `test getSupportedLanguages, same language appears two times, distinct languages list returned`() = runTest {
        // Given
        val expectedResult = listOf(
            LanguageModel("en", "English"),
            LanguageModel("sr", "Serbian")
        )
        val language1: Language = mockk()
        val language2: Language = mockk()
        val language3: Language = mockk()

        every { translationService.listSupportedLanguages() } returns listOf(language1, language2, language3)
        every { language1.code } returns "en"
        every { language1.name } returns "English"
        every { language2.code } returns "sr"
        every { language2.name } returns "Serbian"
        every { language3.code } returns "en-us"
        every { language3.name } returns "English"

        // When
        val response = sut.getSupportedLanguages()

        // Then
        verify { translationService.listSupportedLanguages() }
        assertEquals(expectedResult, response.result())
    }

    @Test
    fun `test translate, exception occurred, error returned`() = runTest {
        // Given
        every { translationService.translate(any<String>(), any(), any()) } throws Exception()

        val sourceLanguageSlot = slot<Translate.TranslateOption>()
        val targetLanguageSlot = slot<Translate.TranslateOption>()

        // When
        val response = sut.translate(
            text = "Rakija",
            sourceLanguage = "sr",
            targetLanguage = "en"
        )

        // Then
        verify {
            translationService.translate(
                "Rakija",
                capture(sourceLanguageSlot),
                capture(targetLanguageSlot)
            )
        }
        assertEquals(sourceLanguageSlot.captured, Translate.TranslateOption.sourceLanguage("sr"))
        assertEquals(targetLanguageSlot.captured, Translate.TranslateOption.targetLanguage("en"))
        assertTrue(response.isError)
    }

    @Test
    fun `test translate, same languages, returned the same text that was passed`() = runTest {
        // When
        val response = sut.translate(
            text = "Rakija",
            sourceLanguage = "sr",
            targetLanguage = "sr"
        )

        // Then
        assertEquals("Rakija", response.result())
        verify { translationService wasNot Called }
    }

    @Test
    fun `test translate, has result, translation returned`() = runTest {
        // Given
        val expectedResult = "Magic potion from the Balkans"
        val translation: Translation = mockk()
        every { translationService.translate(any<String>(), any(), any()) } returns translation
        every { translation.translatedText } returns expectedResult

        val sourceLanguageSlot = slot<Translate.TranslateOption>()
        val targetLanguageSlot = slot<Translate.TranslateOption>()

        // When
        val response = sut.translate(
            text = "Rakija",
            sourceLanguage = "sr",
            targetLanguage = "en"
        )

        // Then
        verify {
            translationService.translate(
                "Rakija",
                capture(sourceLanguageSlot),
                capture(targetLanguageSlot)
            )
        }
        assertEquals(sourceLanguageSlot.captured, Translate.TranslateOption.sourceLanguage("sr"))
        assertEquals(targetLanguageSlot.captured, Translate.TranslateOption.targetLanguage("en"))
        assertEquals(expectedResult, response.result())
    }
}
