package com.filippetrovic.polygloteye.data.repository

import com.filippetrovic.polygloteye.data.datasource.texttospeech.GoogleTextToSpeechDataSource
import com.filippetrovic.polygloteye.data.repository.texttospeech.GoogleTextToSpeechRepository
import com.filippetrovic.polygloteye.data.repository.texttospeech.SpeechResult
import com.filippetrovic.polygloteye.data.repository.texttospeech.SynthesisInputBuilder
import com.filippetrovic.polygloteye.data.repository.texttospeech.VoiceBuilder
import com.filippetrovic.polygloteye.functional.Either
import com.google.api.gax.rpc.ApiException
import com.google.cloud.texttospeech.v1.SynthesisInput
import com.google.cloud.texttospeech.v1.SynthesizeSpeechResponse
import com.google.cloud.texttospeech.v1.Voice
import com.google.cloud.texttospeech.v1.VoiceSelectionParams
import com.google.protobuf.ByteString
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.verify
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test

class GoogleTextToSpeechRepositoryTests {

    @MockK
    private lateinit var dataSource: GoogleTextToSpeechDataSource

    @MockK
    private lateinit var voiceBuilder: VoiceBuilder

    @MockK
    private lateinit var synthesisInputBuilder: SynthesisInputBuilder

    @InjectMockKs
    private lateinit var sut: GoogleTextToSpeechRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `test synthesize, datasource getVoices throws Exception, error returned`() = runTest {
        // Given
        val nativeLanguageCode = "sr-SR"
        val expectedResult = ApiException(Throwable(), mockk(), false)

        every { dataSource.getNativeLanguageCode(any()) } returns nativeLanguageCode
        every { dataSource.getVoices(any()) } throws expectedResult

        // When
        val response = sut.synthesize("Rakija", "sr")

        // Then
        verify { dataSource.getNativeLanguageCode("sr") }
        verify { dataSource.getVoices(nativeLanguageCode) }
        TestCase.assertEquals(expectedResult, response.error())
    }

    @Test
    fun `test synthesize, dataSource synthesize throws Exception, error returned`() = runTest {
        // Given
        val nativeLanguageCode = "sr-SR"
        val voice1: Voice = mockk()
        val voice2: Voice = mockk()
        val voices: List<Voice> = listOf(voice1, voice2)
        val expectedResult = ApiException(Throwable(), mockk(), false)

        every { voice1.name } returns "Standard-A"
        every { voice2.name } returns "Standard-B"
        every { dataSource.getNativeLanguageCode(any()) } returns nativeLanguageCode
        every { dataSource.getVoices(any()) } returns Either.Result(voices)
        every { voiceBuilder.build(any()) } returns mockk()
        every { synthesisInputBuilder.build(any()) } returns mockk()
        every { dataSource.synthesize(any(), any(), any()) } throws expectedResult

        // When
        val response = sut.synthesize("Rakija", "sr")

        // Then
        verify { dataSource.getNativeLanguageCode("sr") }
        verify { dataSource.getVoices(nativeLanguageCode) }
        TestCase.assertEquals(expectedResult, response.error())
    }

    @Test
    fun `test synthesize, no voice list, language not supported returned`() = runTest {
        // Given
        val nativeLanguageCode = "sr-SR"
        val expectedResult = Exception("Language not supported")

        every { dataSource.getNativeLanguageCode(any()) } returns nativeLanguageCode
        every { dataSource.getVoices(any()) } returns Either.Result(null)
        every { voiceBuilder.build(any()) } returns mockk()
        every { synthesisInputBuilder.build(any()) } returns mockk()
        every { dataSource.synthesize(any(), any(), any()) } throws expectedResult

        // When
        val response = sut.synthesize("Rakija", "sr")

        // Then
        verify { dataSource.getNativeLanguageCode("sr") }
        verify { dataSource.getVoices(nativeLanguageCode) }
        TestCase.assertEquals(expectedResult.message, response.error().message)
    }

    @Test
    fun `test synthesize, empty voice list, language not supported returned`() = runTest {
        // Given
        val nativeLanguageCode = "sr-SR"
        val expectedResult = Exception("Language not supported")

        every { dataSource.getNativeLanguageCode(any()) } returns nativeLanguageCode
        every { dataSource.getVoices(any()) } returns Either.Result(emptyList())
        every { voiceBuilder.build(any()) } returns mockk()
        every { synthesisInputBuilder.build(any()) } returns mockk()
        every { dataSource.synthesize(any(), any(), any()) } throws expectedResult

        // When
        val response = sut.synthesize("Rakija", "sr")

        // Then
        verify { dataSource.getNativeLanguageCode("sr") }
        verify { dataSource.getVoices(nativeLanguageCode) }
        TestCase.assertEquals(expectedResult.message, response.error().message)
    }

    @Test
    fun `test synthesize, no standard voice, language not supported returned`() = runTest {
        // Given
        val nativeLanguageCode = "sr-SR"
        val voice1: Voice = mockk()
        val voice2: Voice = mockk()
        val voices: List<Voice> = listOf(voice1, voice2)
        val expectedResult = Exception("Language not supported")

        every { voice1.name } returns "Studio-M"
        every { voice2.name } returns "Neural2-A"
        every { dataSource.getNativeLanguageCode(any()) } returns nativeLanguageCode
        every { dataSource.getVoices(any()) } returns Either.Result(voices)
        every { voiceBuilder.build(any()) } returns mockk()
        every { synthesisInputBuilder.build(any()) } returns mockk()
        every { dataSource.synthesize(any(), any(), any()) } throws expectedResult

        // When
        val response = sut.synthesize("Rakija", "sr")

        // Then
        verify { dataSource.getNativeLanguageCode("sr") }
        verify { dataSource.getVoices(nativeLanguageCode) }
        TestCase.assertEquals(expectedResult.message, response.error().message)
    }

    @Test
    fun `test synthesize, has response, result returned`() = runTest {
        // Given
        val nativeLanguageCode = "sr-SR"
        val sampleRate = 35000
        val voice1: Voice = mockk()
        val voice2: Voice = mockk()
        val voices: List<Voice> = listOf(voice1, voice2)
        val voiceSelectionParams: VoiceSelectionParams = mockk()
        val synthesisInput: SynthesisInput = mockk()
        val synthesizeSpeechResponse: SynthesizeSpeechResponse = mockk()
        val byteString: ByteString = mockk()
        val byteArray = ByteArray(100)
        val expectedResult = SpeechResult(byteArray, sampleRate)

        every { voice1.name } returns "Standard-A"
        every { voice2.name } returns "Standard-B"
        every { voice1.naturalSampleRateHertz } returns sampleRate

        every { dataSource.getNativeLanguageCode(any()) } returns nativeLanguageCode
        every { dataSource.getVoices(any()) } returns Either.Result(voices)
        every { voiceBuilder.build(any()) } returns voiceSelectionParams
        every { synthesisInputBuilder.build(any()) } returns synthesisInput
        every { synthesizeSpeechResponse.audioContent } returns byteString
        every { byteString.toByteArray() } returns byteArray
        every { dataSource.synthesize(any(), any(), any()) } returns Either.Result(synthesizeSpeechResponse)

        // When
        val response = sut.synthesize("Rakija", "sr")

        // Then
        verify { dataSource.getNativeLanguageCode("sr") }
        verify { dataSource.getVoices(nativeLanguageCode) }
        verify { voiceBuilder.build(voice1) }
        verify { synthesisInputBuilder.build("Rakija") }
        verify {
            dataSource.synthesize(synthesisInput, voiceSelectionParams, any())
        }
        TestCase.assertEquals(expectedResult, response.result())
    }
}
