package com.filippetrovic.polygloteye.data.datasource

import com.filippetrovic.polygloteye.data.datasource.texttospeech.GoogleTextToSpeechDataSource
import com.google.cloud.texttospeech.v1.AudioConfig
import com.google.cloud.texttospeech.v1.ListVoicesResponse
import com.google.cloud.texttospeech.v1.SynthesisInput
import com.google.cloud.texttospeech.v1.SynthesizeSpeechResponse
import com.google.cloud.texttospeech.v1.TextToSpeechClient
import com.google.cloud.texttospeech.v1.Voice
import com.google.cloud.texttospeech.v1.VoiceSelectionParams
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.verify
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import org.junit.Before
import org.junit.Test

class GoogleTextToSpeechDataSourceTests {

    @MockK
    private lateinit var textToSpeechClient: TextToSpeechClient

    @InjectMockKs
    private lateinit var sut: GoogleTextToSpeechDataSource

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `test synthesize, exception occurred, error returned`() {
        // Given
        val synthesisInput: SynthesisInput = mockk()
        val voiceSelectionParams: VoiceSelectionParams = mockk()
        val audioConfig: AudioConfig = mockk()

        every { textToSpeechClient.synthesizeSpeech(any(), any(), any()) } throws Exception()

        // When
        val response = sut.synthesize(synthesisInput, voiceSelectionParams, audioConfig)

        // Then
        verify {
            textToSpeechClient.synthesizeSpeech(
                synthesisInput,
                voiceSelectionParams,
                audioConfig
            )
        }
        assertTrue(response.isError)
    }

    @Test
    fun `test synthesize, has result, result returned`() {
        // Given
        val synthesisInput: SynthesisInput = mockk()
        val voiceSelectionParams: VoiceSelectionParams = mockk()
        val audioConfig: AudioConfig = mockk()
        val expectedResult: SynthesizeSpeechResponse = mockk()

        every { textToSpeechClient.synthesizeSpeech(any(), any(), any()) } returns expectedResult

        // When
        val response = sut.synthesize(synthesisInput, voiceSelectionParams, audioConfig)

        // Then
        verify {
            textToSpeechClient.synthesizeSpeech(
                synthesisInput,
                voiceSelectionParams,
                audioConfig
            )
        }
        assertEquals(expectedResult, response.result())
    }

    @Test
    fun `test getVoice, exception occurred, error returned`() {
        // Given
        every { textToSpeechClient.listVoices(any<String>()) } throws Exception()

        // When
        val response = sut.getVoices("sr")

        // Then
        verify { textToSpeechClient.listVoices("sr") }
        assertTrue(response.isError)
    }

    @Test
    fun `test getVoice, has result, result returned`() {
        // Given
        val expectedListVoicesResponse: ListVoicesResponse = mockk()
        val expectedResult: List<Voice> = mockk()

        every { textToSpeechClient.listVoices(any<String>()) } returns expectedListVoicesResponse
        every { expectedListVoicesResponse.voicesList } returns expectedResult

        // When
        val response = sut.getVoices("sr")

        // Then
        verify { textToSpeechClient.listVoices("sr") }
        assertEquals(expectedResult, response.result())
    }

    @Test
    fun `test getNativeLanguageCode, language nl, nl-NL returned`() {
        // When
        val response = sut.getNativeLanguageCode("nl")

        // Then
        assertEquals("nl-NL", response)
    }

    @Test
    fun `test getNativeLanguageCode, language en, en-US returned`() {
        // When
        val response = sut.getNativeLanguageCode("en")

        // Then
        assertEquals("en-US", response)
    }

    @Test
    fun `test getNativeLanguageCode, language fr, fr-FR returned`() {
        // When
        val response = sut.getNativeLanguageCode("fr")

        // Then
        assertEquals("fr-FR", response)
    }

    @Test
    fun `test getNativeLanguageCode, language pt, pt-PT returned`() {
        // When
        val response = sut.getNativeLanguageCode("pt")

        // Then
        assertEquals("pt-PT", response)
    }

    @Test
    fun `test getNativeLanguageCode, language es, es-ES returned`() {
        // When
        val response = sut.getNativeLanguageCode("es")

        // Then
        assertEquals("es-ES", response)
    }

    @Test
    fun `test getNativeLanguageCode, language sr, sr returned`() {
        // When
        val response = sut.getNativeLanguageCode("sr")

        // Then
        assertEquals("sr", response)
    }
}
