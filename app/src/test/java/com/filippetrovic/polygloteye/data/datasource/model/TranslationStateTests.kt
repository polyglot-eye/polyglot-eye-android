package com.filippetrovic.polygloteye.data.datasource.model

import com.filippetrovic.polygloteye.data.datasource.dictionary.TranslationState
import junit.framework.TestCase.assertEquals
import org.junit.Test

class TranslationStateTests {

    @Test
    fun `test getText, state is Loading, null returned`() {
        // Given
        val translationState = TranslationState.Loading

        // When
        val result = translationState.getText()

        assertEquals(null, result)
    }

    @Test
    fun `test getText, state is Value, translation returned`() {
        // Given
        val translationState = TranslationState.Value("Rakija")

        // When
        val result = translationState.getText()

        assertEquals("Rakija", result)
    }
}
