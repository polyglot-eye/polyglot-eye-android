package com.filippetrovic.polygloteye.usecase

import com.filippetrovic.polygloteye.data.player.IPlayer
import com.filippetrovic.polygloteye.data.repository.texttospeech.ITextToSpeechRepository
import com.filippetrovic.polygloteye.data.repository.texttospeech.SpeechResult
import com.filippetrovic.polygloteye.functional.Either
import io.mockk.Called
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import java.lang.Exception

class TextToSpeechUseCaseTests {

    @MockK
    private lateinit var textToSpeechRepository: ITextToSpeechRepository

    @RelaxedMockK
    private lateinit var player: IPlayer

    private lateinit var sut: TextToSpeechUseCase

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        sut = TextToSpeechUseCase(UnconfinedTestDispatcher(), textToSpeechRepository, player)
    }

    @Test
    fun `text textToSpeech, synthesize returns error, error returned`() = runTest {
        // Given
        val exception = Exception()
        coEvery { textToSpeechRepository.synthesize(any(), any()) } returns Either.Error(exception)

        // When
        val response = sut("Rakija", "sr")

        // Then
        coVerify { textToSpeechRepository.synthesize("Rakija", "sr") }
        verify { player wasNot Called }
        assertEquals(exception, response.error())
    }

    @Test
    fun `text textToSpeech, audio is null, no audio data returned`() = runTest {
        // Given
        val speechResult = SpeechResult(audio = null, sampleRate = 35000)
        coEvery { textToSpeechRepository.synthesize(any(), any()) } returns Either.Result(speechResult)

        // When
        val response = sut("Rakija", "sr")

        // Then
        coVerify { textToSpeechRepository.synthesize("Rakija", "sr") }
        verify { player wasNot Called }
        assertEquals("No audio data", response.error().message)
    }

    @Test
    fun `text textToSpeech, has response, result returned`() = runTest {
        // Given
        val byteArray = ByteArray(100)
        val sampleRate = 35000
        val speechResult = SpeechResult(byteArray, sampleRate)

        coEvery { textToSpeechRepository.synthesize(any(), any()) } returns Either.Result(speechResult)

        // When
        val response = sut("Rakija", "sr")

        // Then
        coVerify { textToSpeechRepository.synthesize("Rakija", "sr") }
        verify { player setProperty "sampleRate" value sampleRate }
        coVerify { player.play(byteArray) }
        assertTrue(response.isResult)
    }
}
