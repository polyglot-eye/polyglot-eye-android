@file:OptIn(ExperimentalCoroutinesApi::class, ExperimentalCoroutinesApi::class)

package com.filippetrovic.polygloteye.viewmodel

import android.graphics.Bitmap
import android.graphics.RectF
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.camera.core.ImageProxy
import androidx.camera.core.ImageProxy.PlaneProxy
import com.filippetrovic.polygloteye.R
import com.filippetrovic.polygloteye.data.datasource.translation.ITranslationDataSource
import com.filippetrovic.polygloteye.data.datasource.translation.model.LanguageModel
import com.filippetrovic.polygloteye.data.manager.detection.DetectionResult
import com.filippetrovic.polygloteye.data.manager.detection.IObjectDetectorManager
import com.filippetrovic.polygloteye.data.repository.dictionary.IDictionaryRepository
import com.filippetrovic.polygloteye.data.repository.userpreferences.IUserPreferencesRepository
import com.filippetrovic.polygloteye.functional.Consumable
import com.filippetrovic.polygloteye.functional.Either
import com.filippetrovic.polygloteye.functional.StringHolder
import com.filippetrovic.polygloteye.test.CoroutineTestRule
import com.filippetrovic.polygloteye.ui.objectdetection.model.DetectionResultUiModel
import com.filippetrovic.polygloteye.ui.objectdetection.model.DetectionUiModel
import com.filippetrovic.polygloteye.ui.objectdetection.model.ObjectDetectionViewState
import com.filippetrovic.polygloteye.usecase.TextToSpeechUseCase
import io.mockk.Called
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.coVerifySequence
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.verify
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runCurrent
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.tensorflow.lite.support.label.Category
import org.tensorflow.lite.task.vision.detector.Detection

class ObjectDetectionViewModelTests {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutineTestRule = CoroutineTestRule(StandardTestDispatcher())

    @MockK
    private lateinit var objectDetectorManager: IObjectDetectorManager

    @MockK
    private lateinit var translationDataSource: ITranslationDataSource

    @RelaxedMockK
    private lateinit var userPreferences: IUserPreferencesRepository

    @MockK
    private lateinit var dictionaryRepository: IDictionaryRepository

    @MockK
    private lateinit var textToSpeechUseCase: TextToSpeechUseCase

    @InjectMockKs
    private lateinit var sut: ObjectDetectionViewModel

    private val initialState = ObjectDetectionViewState()

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `test initializeObjectDetector, has error, state updated with error`() = runTest {
        // Given
        every { objectDetectorManager.initialize() } returns Either.Error(Exception("Error message"))
        val expectedState = initialState.copy(
            errorMessage = Consumable(StringHolder("Error message"))
        )

        val states = mutableListOf<ObjectDetectionViewState>()
        sut.state
            .onEach(states::add)
            .launchIn(CoroutineScope(UnconfinedTestDispatcher(testScheduler)))

        // When
        sut.initializeObjectDetector()

        // Then
        verify { objectDetectorManager.initialize() }
        assertEquals(listOf(initialState, expectedState), states)
    }

    @Test
    fun `test initializeObjectDetector, has result, object detector initialized`() = runTest {
        // Given
        every { objectDetectorManager.initialize() } returns Either.Result(Unit)

        val states = mutableListOf<ObjectDetectionViewState>()
        sut.state
            .onEach(states::add)
            .launchIn(CoroutineScope(UnconfinedTestDispatcher(testScheduler)))

        // When
        sut.initializeObjectDetector()

        // Then
        verify { objectDetectorManager.initialize() }
        assertEquals(listOf(initialState), states)
    }

    @Test
    fun `test getSupportedLanguages, hasResponse, state updated with supportedLanguages`() = runTest {
        // Given
        val supportedLanguages = listOf(
            LanguageModel("sr", "Serbian"),
            LanguageModel("en", "English")
        )
        val expectedStates = listOf(
            initialState,
            initialState.copy(supportedLanguages = supportedLanguages)
        )

        coEvery { translationDataSource.getSupportedLanguages() }.returns(
            Either.Result(supportedLanguages)
        )

        val states = mutableListOf<ObjectDetectionViewState>()
        sut.state
            .onEach(states::add)
            .launchIn(CoroutineScope(UnconfinedTestDispatcher(testScheduler)))

        // When
        sut.getSupportedLanguages()
        advanceUntilIdle()
        runCurrent()

        // Then
        assertEquals(expectedStates, states)
    }

    @Test
    fun `test getSupportedLanguages, error occurs until there is response, state updated with supportedLanguages`() = runTest {
        // Given
        val supportedLanguages = listOf(
            LanguageModel("sr", "Serbian"),
            LanguageModel("en", "English")
        )
        val state2 = initialState.copy(errorMessage = Consumable(StringHolder(R.string.connection_error)))
        val state3 = state2.copy(supportedLanguages = supportedLanguages)
        val expectedStates = listOf(initialState, state2, state3)

        coEvery { translationDataSource.getSupportedLanguages() }.returnsMany(
            Either.Error(Exception()),
            Either.Error(Exception()),
            Either.Result(supportedLanguages)
        )

        val states = mutableListOf<ObjectDetectionViewState>()
        sut.state
            .onEach(states::add)
            .launchIn(CoroutineScope(UnconfinedTestDispatcher(testScheduler)))

        // When
        sut.getSupportedLanguages()
        advanceUntilIdle()
        runCurrent()

        // Then
        assertEquals(10000, testScheduler.currentTime)
        assertEquals(expectedStates, states)
    }

    @Test
    fun `test getLastUsedLanguages, no languages saved, state updated with default languages`() = runTest {
        // Given
        val expectedState = initialState.copy(
            sourceLanguage = LanguageModel("en", "English"),
            targetLanguage = LanguageModel("en", "English")
        )
        coEvery { userPreferences.getSourceLanguage() } returns flow { emit(null) }
        coEvery { userPreferences.getTargetLanguage() } returns flow { emit(null) }

        val states = mutableListOf<ObjectDetectionViewState>()
        sut.state
            .onEach(states::add)
            .launchIn(CoroutineScope(UnconfinedTestDispatcher(testScheduler)))

        // When
        sut.getLastUsedLanguages()
        runCurrent()

        // Then
        assertEquals(listOf(expectedState), states)
    }

    @Test
    fun `test getLastUsedLanguages, has languages saved, state updated with saved languages`() = runTest {
        // Given
        val serbianLanguage = LanguageModel("sr", "Serbian")
        val spanishLanguage = LanguageModel("es", "Spanish")
        val expectedState = initialState.copy(
            sourceLanguage = serbianLanguage,
            targetLanguage = spanishLanguage
        )
        coEvery { userPreferences.getSourceLanguage() } returns flow { emit(serbianLanguage) }
        coEvery { userPreferences.getTargetLanguage() } returns flow { emit(spanishLanguage) }

        val states = mutableListOf<ObjectDetectionViewState>()
        sut.state
            .onEach(states::add)
            .launchIn(CoroutineScope(UnconfinedTestDispatcher(testScheduler)))

        // When
        sut.getLastUsedLanguages()
        runCurrent()

        // Then
        assertEquals(listOf(initialState, expectedState), states)
    }

    @Test
    fun `test detectObjects, error detecting object, state updated with error`() = runTest {
        // Given
        mockkStatic(Bitmap::class)
        val bitmap: Bitmap = mockk(relaxed = true)
        val imageProxy: ImageProxy = mockk(relaxed = true)
        val imageProxyPlane: PlaneProxy = mockk()
        val imageProxyPlanes: Array<PlaneProxy> = arrayOf(imageProxyPlane)
        val exception = Exception("Error message")
        val expectedState = initialState.copy(
            errorMessage = Consumable(StringHolder("Error message"))
        )

        every { imageProxy.width } returns 100
        every { imageProxy.height } returns 100
        every { imageProxy.planes } returns imageProxyPlanes
        every { imageProxyPlane.buffer } returns mockk()
        every { Bitmap.createBitmap(any(), any(), any()) } returns bitmap
        every { objectDetectorManager.detect(any(), any()) } returns Either.Error(exception)

        val states = mutableListOf<ObjectDetectionViewState>()
        sut.state
            .onEach(states::add)
            .launchIn(CoroutineScope(UnconfinedTestDispatcher(testScheduler)))

        // When
        sut.detectObjects(imageProxy)

        // Then
        verify { objectDetectorManager.detect(bitmap, any()) }
        assertEquals(listOf(initialState, expectedState), states)
    }

    @Test
    fun `test detectObjects, no objects detected, state updated with no objects detected`() = runTest {
        // Given
        mockkStatic(Bitmap::class)

        val rakijaSerbian = "Rakija"
        val rakijaEnglish = "Magic potion from the Balkans"
        val bitmap: Bitmap = mockk(relaxed = true)
        val imageProxy: ImageProxy = mockk(relaxed = true)
        val imageProxyPlane: PlaneProxy = mockk()
        val imageProxyPlanes: Array<PlaneProxy> = arrayOf(imageProxyPlane)
        val detection: Detection = mockk()
        val category: Category = mockk()
        val boundingBox: RectF = mockk()

        val detections = DetectionResult()
        val expectedState = initialState.copy(
            detectionResult = DetectionResultUiModel()
        )

        every { imageProxy.width } returns 200
        every { imageProxy.height } returns 100
        every { imageProxy.planes } returns imageProxyPlanes
        every { imageProxyPlane.buffer } returns mockk()
        every { Bitmap.createBitmap(any(), any(), any()) } returns bitmap
        every { detection.boundingBox } returns boundingBox
        every { detection.categories } returns listOf(category)
        every { category.label } returns rakijaEnglish
        every { dictionaryRepository.getTranslation(any(), any()) }.returnsMany(
            rakijaSerbian,
            rakijaEnglish
        )

        every { objectDetectorManager.detect(any(), any()) } returns Either.Result(detections)

        val states = mutableListOf<ObjectDetectionViewState>()
        sut.state
            .onEach(states::add)
            .launchIn(CoroutineScope(UnconfinedTestDispatcher(testScheduler)))

        // When
        sut.detectObjects(imageProxy)

        // Then
        verify { objectDetectorManager.detect(bitmap, any()) }
        assertEquals(listOf(expectedState), states)
    }

    @Test
    fun `test detectObjects, one object detected, state updated with one object detection`() = runTest {
        // Given
        mockkStatic(Bitmap::class)

        val serbianLanguageModel = LanguageModel("sr", "Serbian")
        val rakijaSerbian = "Rakija"
        val rakijaEnglish = "Magic potion from the Balkans"
        val bitmap: Bitmap = mockk(relaxed = true)
        val imageProxy: ImageProxy = mockk(relaxed = true)
        val imageProxyPlane: PlaneProxy = mockk()
        val imageProxyPlanes: Array<PlaneProxy> = arrayOf(imageProxyPlane)
        val detection: Detection = mockk()
        val category: Category = mockk()
        val boundingBox: RectF = mockk()

        val detections = DetectionResult(
            detections = listOf(detection),
            inferenceTime = 10,
            imageHeight = 200,
            imageWidth = 100
        )
        val detectionUiModel = DetectionUiModel(
            boundingBox,
            rakijaSerbian,
            rakijaEnglish
        )
        val sourceLanguageChangedState = initialState.copy(
            sourceLanguage = serbianLanguageModel
        )
        val expectedState = sourceLanguageChangedState.copy(
            detectionResult = DetectionResultUiModel(
                listOf(detectionUiModel),
                10,
                200,
                100
            )
        )

        every { imageProxy.width } returns 200
        every { imageProxy.height } returns 100
        every { imageProxy.planes } returns imageProxyPlanes
        every { imageProxyPlane.buffer } returns mockk()
        every { Bitmap.createBitmap(any(), any(), any()) } returns bitmap
        every { detection.boundingBox } returns boundingBox
        every { detection.categories } returns listOf(category)
        every { category.label } returns rakijaEnglish
        every { dictionaryRepository.getTranslation(any(), any()) }.returnsMany(
            rakijaSerbian,
            rakijaEnglish
        )

        every { objectDetectorManager.detect(any(), any()) } returns Either.Result(detections)

        val states = mutableListOf<ObjectDetectionViewState>()
        sut.state
            .onEach(states::add)
            .launchIn(CoroutineScope(UnconfinedTestDispatcher(testScheduler)))

        // When
        sut.setSourceLanguage(serbianLanguageModel)
        sut.detectObjects(imageProxy)

        // Then
        verify { objectDetectorManager.detect(bitmap, any()) }
        verify { dictionaryRepository.getTranslation(rakijaEnglish, "sr") }
        verify { dictionaryRepository.getTranslation(rakijaEnglish, "en") }
        assertEquals(listOf(initialState, sourceLanguageChangedState, expectedState), states)
    }

    @Test
    fun `test detectObjects, two objects detected, state updated with two objects detection`() = runTest {
        // Given
        mockkStatic(Bitmap::class)

        val serbianLanguageModel = LanguageModel("sr", "Serbian")
        val rakijaSerbian = "Rakija"
        val rakijaEnglish = "Magic potion from the Balkans"
        val pivoSerbian = "Pivo"
        val pivoEnglish = "Beer"
        val bitmap: Bitmap = mockk(relaxed = true)
        val imageProxy: ImageProxy = mockk(relaxed = true)
        val imageProxyPlane: PlaneProxy = mockk()
        val imageProxyPlanes: Array<PlaneProxy> = arrayOf(imageProxyPlane)
        val detection1: Detection = mockk()
        val detection2: Detection = mockk()
        val category1: Category = mockk()
        val boundingBox1: RectF = mockk()
        val category2: Category = mockk()
        val boundingBox2: RectF = mockk()

        val detections = DetectionResult(
            detections = listOf(detection1, detection2),
            inferenceTime = 10,
            imageHeight = 200,
            imageWidth = 100
        )
        val detectionUiModel1 = DetectionUiModel(
            boundingBox1,
            rakijaSerbian,
            rakijaEnglish
        )
        val detectionUiModel2 = DetectionUiModel(
            boundingBox2,
            pivoSerbian,
            pivoEnglish
        )
        val sourceLanguageChangedState = initialState.copy(
            sourceLanguage = serbianLanguageModel
        )
        val expectedState = sourceLanguageChangedState.copy(
            detectionResult = DetectionResultUiModel(
                listOf(detectionUiModel1, detectionUiModel2),
                10,
                200,
                100
            )
        )

        every { imageProxy.width } returns 200
        every { imageProxy.height } returns 100
        every { imageProxy.planes } returns imageProxyPlanes
        every { imageProxyPlane.buffer } returns mockk()
        every { Bitmap.createBitmap(any(), any(), any()) } returns bitmap
        every { detection1.boundingBox } returns boundingBox1
        every { detection1.categories } returns listOf(category1)
        every { detection2.boundingBox } returns boundingBox2
        every { detection2.categories } returns listOf(category2)
        every { category1.label } returns rakijaEnglish
        every { category2.label } returns pivoEnglish
        every { dictionaryRepository.getTranslation(any(), any()) }.returnsMany(
            rakijaSerbian,
            rakijaEnglish,
            pivoSerbian,
            pivoEnglish
        )

        every { objectDetectorManager.detect(any(), any()) } returns Either.Result(detections)

        val states = mutableListOf<ObjectDetectionViewState>()
        sut.state
            .onEach(states::add)
            .launchIn(CoroutineScope(UnconfinedTestDispatcher(testScheduler)))

        // When
        sut.setSourceLanguage(serbianLanguageModel)
        sut.detectObjects(imageProxy)

        // Then
        verify { objectDetectorManager.detect(bitmap, any()) }
        verify { dictionaryRepository.getTranslation(rakijaEnglish, "sr") }
        verify { dictionaryRepository.getTranslation(rakijaEnglish, "en") }
        verify { dictionaryRepository.getTranslation(pivoEnglish, "sr") }
        verify { dictionaryRepository.getTranslation(pivoEnglish, "en") }
        assertEquals(listOf(initialState, sourceLanguageChangedState, expectedState), states)
    }

    @Test
    fun `test setSourceLanguage`() = runTest {
        // Given
        val serbianLanguageModel = LanguageModel("sr", "Serbian")
        val expectedState = initialState.copy(
            sourceLanguage = serbianLanguageModel
        )

        val states = mutableListOf<ObjectDetectionViewState>()
        sut.state
            .onEach(states::add)
            .launchIn(CoroutineScope(UnconfinedTestDispatcher(testScheduler)))

        // When
        sut.setSourceLanguage(serbianLanguageModel)
        runCurrent()

        // Then
        coVerify { userPreferences.updateSourceLanguage(serbianLanguageModel) }
        assertEquals(listOf(initialState, expectedState), states)
    }

    @Test
    fun `test swapLanguages`() = runTest {
        // Given
        val serbianLanguageModel = LanguageModel("sr", "Serbian")
        val spanishLanguageModel = LanguageModel("es", "Spanish")
        val sourceLanguageChangedState = initialState.copy(
            sourceLanguage = serbianLanguageModel
        )
        val targetLanguageChangedState = sourceLanguageChangedState.copy(
            targetLanguage = spanishLanguageModel
        )
        val expectedState = targetLanguageChangedState.copy(
            sourceLanguage = spanishLanguageModel,
            targetLanguage = serbianLanguageModel
        )

        val states = mutableListOf<ObjectDetectionViewState>()
        sut.state
            .onEach(states::add)
            .launchIn(CoroutineScope(UnconfinedTestDispatcher(testScheduler)))

        // When
        sut.setSourceLanguage(serbianLanguageModel)
        sut.setTargetLanguage(spanishLanguageModel)
        sut.swapLanguages()
        runCurrent()

        // Then
        coVerifySequence {
            userPreferences.updateSourceLanguage(serbianLanguageModel)
            userPreferences.updateTargetLanguage(spanishLanguageModel)
            userPreferences.updateSourceLanguage(spanishLanguageModel)
            userPreferences.updateTargetLanguage(serbianLanguageModel)
        }
        assertEquals(
            listOf(
                initialState,
                sourceLanguageChangedState,
                targetLanguageChangedState,
                expectedState
            ),
            states
        )
    }

    @Test
    fun `test synthesize, empty label, nothing happens`() = runTest {
        // Given
        val detection = DetectionUiModel(
            boundingBox = mockk(),
            sourceLanguageLabel = "",
            targetLanguageLabel = ""
        )

        val states = mutableListOf<ObjectDetectionViewState>()
        sut.state
            .onEach(states::add)
            .launchIn(CoroutineScope(UnconfinedTestDispatcher(testScheduler)))

        // When
        sut.synthesize(detection)
        runCurrent()

        // Then
        verify { textToSpeechUseCase wasNot Called }
        assertEquals(listOf(initialState), states)
    }

    @Test
    fun `test synthesize, error occurred, state updated with error`() = runTest {
        // Given
        val serbianLanguageModel = LanguageModel("sr", "Serbian")
        val rakijaSerbian = "Rakija"
        val rakijaEnglish = "Magic potion from the Balkans"
        val detection = DetectionUiModel(
            boundingBox = mockk(),
            sourceLanguageLabel = rakijaEnglish,
            targetLanguageLabel = rakijaSerbian
        )
        val exception = Exception("The error message")
        val targetLanguageChangedState = initialState.copy(
            targetLanguage = serbianLanguageModel
        )
        val expectedState = targetLanguageChangedState.copy(
            errorMessage = Consumable(StringHolder("The error message"))
        )

        coEvery { textToSpeechUseCase(any(), any()) } returns Either.Error(exception)

        val states = mutableListOf<ObjectDetectionViewState>()
        sut.state
            .onEach(states::add)
            .launchIn(CoroutineScope(UnconfinedTestDispatcher(testScheduler)))

        // When
        sut.setTargetLanguage(serbianLanguageModel)
        sut.synthesize(detection)
        runCurrent()

        // Then
        coVerify { textToSpeechUseCase(rakijaSerbian, "sr") }
        assertEquals(listOf(initialState, targetLanguageChangedState, expectedState), states)
    }

    @Test
    fun `test synthesize, text to speech has result`() = runTest {
        // Given
        val serbianLanguageModel = LanguageModel("sr", "Serbian")
        val rakijaSerbian = "Rakija"
        val rakijaEnglish = "Magic potion from the Balkans"
        val detection = DetectionUiModel(
            boundingBox = mockk(),
            sourceLanguageLabel = rakijaEnglish,
            targetLanguageLabel = rakijaSerbian
        )
        val targetLanguageChangedState = initialState.copy(
            targetLanguage = serbianLanguageModel
        )

        coEvery { textToSpeechUseCase(any(), any()) } returns Either.Result(Unit)

        val states = mutableListOf<ObjectDetectionViewState>()
        sut.state
            .onEach(states::add)
            .launchIn(CoroutineScope(UnconfinedTestDispatcher(testScheduler)))

        // When
        sut.setTargetLanguage(serbianLanguageModel)
        sut.synthesize(detection)
        runCurrent()

        // Then
        coVerify { textToSpeechUseCase(rakijaSerbian, "sr") }
        assertEquals(listOf(initialState, targetLanguageChangedState), states)
    }

    @Test
    fun `test clearDetections`() = runTest {
        // Given
        mockkStatic(Bitmap::class)

        val serbianLanguageModel = LanguageModel("sr", "Serbian")
        val rakijaSerbian = "Rakija"
        val rakijaEnglish = "Magic potion from the Balkans"
        val bitmap: Bitmap = mockk(relaxed = true)
        val imageProxy: ImageProxy = mockk(relaxed = true)
        val imageProxyPlane: PlaneProxy = mockk()
        val imageProxyPlanes: Array<PlaneProxy> = arrayOf(imageProxyPlane)
        val detection: Detection = mockk()
        val category: Category = mockk()
        val boundingBox: RectF = mockk()

        val detections = DetectionResult(
            detections = listOf(detection),
            inferenceTime = 10,
            imageHeight = 200,
            imageWidth = 100
        )
        val detectionUiModel = DetectionUiModel(
            boundingBox,
            rakijaSerbian,
            rakijaEnglish
        )
        val sourceLanguageChangedState = initialState.copy(
            sourceLanguage = serbianLanguageModel
        )
        val expectedState = sourceLanguageChangedState.copy(
            detectionResult = DetectionResultUiModel(
                listOf(detectionUiModel),
                10,
                200,
                100
            )
        )

        every { imageProxy.width } returns 200
        every { imageProxy.height } returns 100
        every { imageProxy.planes } returns imageProxyPlanes
        every { imageProxyPlane.buffer } returns mockk()
        every { Bitmap.createBitmap(any(), any(), any()) } returns bitmap
        every { detection.boundingBox } returns boundingBox
        every { detection.categories } returns listOf(category)
        every { category.label } returns rakijaEnglish
        every { dictionaryRepository.getTranslation(any(), any()) }.returnsMany(
            rakijaSerbian,
            rakijaEnglish
        )

        every { objectDetectorManager.detect(any(), any()) } returns Either.Result(detections)

        val states = mutableListOf<ObjectDetectionViewState>()
        sut.state
            .onEach(states::add)
            .launchIn(CoroutineScope(UnconfinedTestDispatcher(testScheduler)))

        // When
        sut.setSourceLanguage(serbianLanguageModel)
        sut.detectObjects(imageProxy)
        sut.clearDetections()

        // Then
        verify { objectDetectorManager.detect(bitmap, any()) }
        verify { dictionaryRepository.getTranslation(rakijaEnglish, "sr") }
        verify { dictionaryRepository.getTranslation(rakijaEnglish, "en") }
        assertEquals(
            listOf(
                initialState,
                sourceLanguageChangedState,
                expectedState,
                sourceLanguageChangedState
            ),
            states
        )
    }
}
