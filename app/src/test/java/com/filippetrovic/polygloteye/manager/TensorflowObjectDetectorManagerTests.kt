package com.filippetrovic.polygloteye.manager

import android.content.Context
import android.os.SystemClock
import com.filippetrovic.polygloteye.R
import com.filippetrovic.polygloteye.data.manager.detection.DetectionResult
import com.filippetrovic.polygloteye.data.manager.detection.ImageProcessorBuilder
import com.filippetrovic.polygloteye.data.manager.detection.TensorflowObjectDetectorManager
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.verify
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.tensorflow.lite.support.image.ImageProcessor
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.task.vision.detector.Detection
import org.tensorflow.lite.task.vision.detector.ObjectDetector

class TensorflowObjectDetectorManagerTests {

    @MockK(relaxed = true)
    private lateinit var context: Context

    @MockK
    lateinit var imageProcessorBuilder: ImageProcessorBuilder

    @InjectMockKs
    private lateinit var sut: TensorflowObjectDetectorManager

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `test initialize, has error, error returned`() = runTest {
        // Given
        mockkStatic(ObjectDetector::class)
        val errorMessage = "Object detector failed to initialize."

        every { ObjectDetector.createFromFileAndOptions(any(), any(), any()) } throws IllegalStateException()
        every { context.getString(any()) } returns errorMessage

        // When
        val response = sut.initialize()

        // Then
        verify { context.getString(R.string.object_detector_initialization_failure_error_message) }
        assertEquals(errorMessage, response.error().message)
    }

    @Test
    fun `test initialize, object detector model initialized, success returned`() = runTest {
        // Given
        mockkStatic(ObjectDetector::class)
        val errorMessage = "Object detector failed to initialize."

        every { ObjectDetector.createFromFileAndOptions(any(), any(), any()) } returns mockk()
        every { context.getString(any()) } returns errorMessage

        // When
        val response = sut.initialize()

        // Then
        assertTrue(response.isResult)
    }

    @Test
    fun `test detect, detections are null, result with no detections returned`() = runTest {
        // Given
        mockkStatic(ObjectDetector::class)
        mockkStatic(SystemClock::class)
        mockkStatic(TensorImage::class)
        mockkStatic(ImageProcessor::class)
        mockkStatic(ImageProcessor.Builder::class)
        val objectDetector: ObjectDetector = mockk()
        val imageProcessor: ImageProcessor = mockk(relaxed = true)
        val tensorImage: TensorImage = mockk()
        val detections: List<Detection>? = null
        val expectedResult = DetectionResult(
            emptyList(),
            10,
            200,
            100
        )

        every { ObjectDetector.createFromFileAndOptions(any(), any(), any()) } returns objectDetector
        every { SystemClock.uptimeMillis() } returnsMany listOf(1000, 1010)
        every { imageProcessorBuilder.build(any()) } returns imageProcessor
        every { imageProcessor.process(any()) } returns tensorImage
        every { tensorImage.height } returns 200
        every { tensorImage.width } returns 100
        every { TensorImage.fromBitmap(any()) } returns mockk()
        every { objectDetector.detect(any<TensorImage>()) } returns detections

        // When
        sut.initialize()
        val response = sut.detect(mockk(), 0)

        // Then
        assertEquals(expectedResult, response.result())
    }

    @Test
    fun `test detect, no detected objects, result with no detections returned`() = runTest {
        // Given
        mockkStatic(ObjectDetector::class)
        mockkStatic(SystemClock::class)
        mockkStatic(TensorImage::class)
        mockkStatic(ImageProcessor::class)
        mockkStatic(ImageProcessor.Builder::class)
        val objectDetector: ObjectDetector = mockk()
        val imageProcessor: ImageProcessor = mockk(relaxed = true)
        val tensorImage: TensorImage = mockk()
        val detections: List<Detection> = emptyList()
        val expectedResult = DetectionResult(
            detections,
            10,
            200,
            100
        )

        every { ObjectDetector.createFromFileAndOptions(any(), any(), any()) } returns objectDetector
        every { SystemClock.uptimeMillis() } returnsMany listOf(1000, 1010)
        every { imageProcessorBuilder.build(any()) } returns imageProcessor
        every { imageProcessor.process(any()) } returns tensorImage
        every { tensorImage.height } returns 200
        every { tensorImage.width } returns 100
        every { TensorImage.fromBitmap(any()) } returns mockk()
        every { objectDetector.detect(any<TensorImage>()) } returns detections

        // When
        sut.initialize()
        val response = sut.detect(mockk(), 0)

        // Then
        assertEquals(expectedResult, response.result())
    }

    @Test
    fun `test detect, detected one object, result with one detection returned`() = runTest {
        // Given
        mockkStatic(ObjectDetector::class)
        mockkStatic(SystemClock::class)
        mockkStatic(TensorImage::class)
        mockkStatic(ImageProcessor::class)
        mockkStatic(ImageProcessor.Builder::class)
        val objectDetector: ObjectDetector = mockk()
        val imageProcessor: ImageProcessor = mockk(relaxed = true)
        val tensorImage: TensorImage = mockk()
        val detections: List<Detection> = listOf(mockk())
        val expectedResult = DetectionResult(
            detections,
            10,
            200,
            100
        )

        every { ObjectDetector.createFromFileAndOptions(any(), any(), any()) } returns objectDetector
        every { SystemClock.uptimeMillis() } returnsMany listOf(1000, 1010)
        every { imageProcessorBuilder.build(any()) } returns imageProcessor
        every { imageProcessor.process(any()) } returns tensorImage
        every { tensorImage.height } returns 200
        every { tensorImage.width } returns 100
        every { TensorImage.fromBitmap(any()) } returns mockk()
        every { objectDetector.detect(any<TensorImage>()) } returns detections

        // When
        sut.initialize()
        val response = sut.detect(mockk(), 0)

        // Then
        assertEquals(expectedResult, response.result())
    }

    @Test
    fun `test detect, detected two objects, result with two detections returned`() = runTest {
        // Given
        mockkStatic(ObjectDetector::class)
        mockkStatic(SystemClock::class)
        mockkStatic(TensorImage::class)
        mockkStatic(ImageProcessor::class)
        mockkStatic(ImageProcessor.Builder::class)
        val objectDetector: ObjectDetector = mockk()
        val imageProcessor: ImageProcessor = mockk(relaxed = true)
        val tensorImage: TensorImage = mockk()
        val detections: List<Detection> = listOf(mockk(), mockk())
        val expectedResult = DetectionResult(
            detections,
            10,
            200,
            100
        )

        every { ObjectDetector.createFromFileAndOptions(any(), any(), any()) } returns objectDetector
        every { SystemClock.uptimeMillis() } returnsMany listOf(1000, 1010)
        every { imageProcessorBuilder.build(any()) } returns imageProcessor
        every { imageProcessor.process(any()) } returns tensorImage
        every { tensorImage.height } returns 200
        every { tensorImage.width } returns 100
        every { TensorImage.fromBitmap(any()) } returns mockk()
        every { objectDetector.detect(any<TensorImage>()) } returns detections

        // When
        sut.initialize()
        val response = sut.detect(mockk(), 0)

        // Then
        assertEquals(expectedResult, response.result())
    }

    @Test
    fun `test detect, detected two objects - one of which is null, result with one detection returned`() = runTest {
        // Given
        mockkStatic(ObjectDetector::class)
        mockkStatic(SystemClock::class)
        mockkStatic(TensorImage::class)
        mockkStatic(ImageProcessor::class)
        mockkStatic(ImageProcessor.Builder::class)
        val objectDetector: ObjectDetector = mockk()
        val imageProcessor: ImageProcessor = mockk(relaxed = true)
        val tensorImage: TensorImage = mockk()
        val detection: Detection = mockk()
        val detections: List<Detection?> = listOf(detection, null)
        val expectedResult = DetectionResult(
            listOf(detection),
            10,
            200,
            100
        )

        every { ObjectDetector.createFromFileAndOptions(any(), any(), any()) } returns objectDetector
        every { SystemClock.uptimeMillis() } returnsMany listOf(1000, 1010)
        every { imageProcessorBuilder.build(any()) } returns imageProcessor
        every { imageProcessor.process(any()) } returns tensorImage
        every { tensorImage.height } returns 200
        every { tensorImage.width } returns 100
        every { TensorImage.fromBitmap(any()) } returns mockk()
        every { objectDetector.detect(any<TensorImage>()) } returns detections

        // When
        sut.initialize()
        val response = sut.detect(mockk(), 0)

        // Then
        assertEquals(expectedResult, response.result())
    }

    @Test
    fun `test detect, detector not initialized, initialize detector and return result`() = runTest {
        // Given
        mockkStatic(ObjectDetector::class)
        mockkStatic(SystemClock::class)
        mockkStatic(TensorImage::class)
        mockkStatic(ImageProcessor::class)
        mockkStatic(ImageProcessor.Builder::class)
        val objectDetector: ObjectDetector = mockk()
        val imageProcessor: ImageProcessor = mockk(relaxed = true)
        val tensorImage: TensorImage = mockk()
        val detections: List<Detection> = listOf(mockk())
        val expectedResult = DetectionResult(
            detections,
            10,
            200,
            100
        )

        every { ObjectDetector.createFromFileAndOptions(any(), any(), any()) } returns objectDetector
        every { SystemClock.uptimeMillis() } returnsMany listOf(1000, 1010)
        every { imageProcessorBuilder.build(any()) } returns imageProcessor
        every { imageProcessor.process(any()) } returns tensorImage
        every { tensorImage.height } returns 200
        every { tensorImage.width } returns 100
        every { TensorImage.fromBitmap(any()) } returns mockk()
        every { objectDetector.detect(any<TensorImage>()) } returns detections

        // When
        val response = sut.detect(mockk(), 0)

        // Then
        assertEquals(expectedResult, response.result())
    }

    @Test
    fun `test detect, detector not initialized, initialize detector returns error, error returned`() = runTest {
        // Given
        mockkStatic(ObjectDetector::class)
        val errorMessage = "Object detector failed to initialize."

        every { ObjectDetector.createFromFileAndOptions(any(), any(), any()) } throws IllegalStateException()
        every { context.getString(any()) } returns errorMessage

        // When
        val response = sut.detect(mockk(), 0)

        // Then
        verify { context.getString(R.string.object_detector_initialization_failure_error_message) }
        assertEquals(errorMessage, response.error().message)
    }
}
