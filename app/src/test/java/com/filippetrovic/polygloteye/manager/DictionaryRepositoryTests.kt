@file:OptIn(ExperimentalCoroutinesApi::class, ExperimentalCoroutinesApi::class)

package com.filippetrovic.polygloteye.manager

import com.filippetrovic.polygloteye.data.datasource.dictionary.IDictionaryDataSource
import com.filippetrovic.polygloteye.data.datasource.dictionary.TranslationState
import com.filippetrovic.polygloteye.data.datasource.translation.ITranslationDataSource
import com.filippetrovic.polygloteye.data.repository.dictionary.DictionaryRepository
import com.filippetrovic.polygloteye.functional.Either
import com.filippetrovic.polygloteye.test.CoroutineTestRule
import io.mockk.Called
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runCurrent
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.lang.Exception

class DictionaryRepositoryTests {

    @get:Rule
    var coroutineTestRule = CoroutineTestRule(StandardTestDispatcher())

    @RelaxedMockK
    lateinit var dictionaryDataSource: IDictionaryDataSource

    @MockK
    lateinit var translationDataSource: ITranslationDataSource

    private lateinit var sut: DictionaryRepository

    private val rakijaSerbian = "Rakija"
    private val rakijaEnglish = "Magic potion from the Balkans"
    private val serbianLanguageCode = "sr"
    private val englishLanguageCode = "en"
    private val repeatIntervalOnError = 1000L

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        sut = DictionaryRepository(
            CoroutineScope(StandardTestDispatcher()),
            UnconfinedTestDispatcher(),
            dictionaryDataSource,
            translationDataSource
        )
    }

    @Test
    fun `test getTranslation, translation is already stored, translation returned`() {
        // Given
        every { dictionaryDataSource.getTranslation(any(), any()) }.returns(
            TranslationState.Value(rakijaSerbian)
        )

        // When
        val response = sut.getTranslation(rakijaEnglish, serbianLanguageCode)

        // Then
        assertEquals(rakijaSerbian, response)
        verify { dictionaryDataSource.getTranslation(rakijaEnglish, serbianLanguageCode) }
        verify { translationDataSource wasNot Called }
    }

    @Test
    fun `test getTranslation, translate throws exception, null returned and translation removed after 1s`() = runTest {
        // Given
        every { dictionaryDataSource.getTranslation(any(), any()) } returns null
        coEvery { translationDataSource.translate(any(), any(), any()) }.returns(
            Either.Error(Exception())
        )

        // When
        val response = sut.getTranslation(rakijaEnglish, serbianLanguageCode)

        // Then
        verify(exactly = 1) { dictionaryDataSource.getTranslation(rakijaEnglish, serbianLanguageCode) }
        verify(exactly = 1) { dictionaryDataSource.setLoadingTranslation(rakijaEnglish, serbianLanguageCode) }
        coVerify(exactly = 1) { translationDataSource.translate(rakijaEnglish, englishLanguageCode, serbianLanguageCode) }
        verify(exactly = 0) { dictionaryDataSource.removeTranslation(rakijaEnglish, serbianLanguageCode) }
        assertEquals(null, response)

        // When
        advanceTimeBy(repeatIntervalOnError)
        runCurrent()

        // Then
        verify(exactly = 1) { dictionaryDataSource.removeTranslation(rakijaEnglish, serbianLanguageCode) }
        verify(exactly = 0) { dictionaryDataSource.putTranslation(rakijaEnglish, serbianLanguageCode, rakijaSerbian) }
    }

    @Test
    fun `test getTranslation called 2 times, translate throws exception, null returned and translation removed after 1s`() = runTest {
        // Given
        every { dictionaryDataSource.getTranslation(any(), any()) } returnsMany listOf(null, TranslationState.Loading)
        coEvery { translationDataSource.translate(any(), any(), any()) }.returns(
            Either.Error(Exception())
        )

        // When
        sut.getTranslation(rakijaEnglish, serbianLanguageCode)
        val response = sut.getTranslation(rakijaEnglish, serbianLanguageCode)

        // Then
        verify(exactly = 2) { dictionaryDataSource.getTranslation(rakijaEnglish, serbianLanguageCode) }
        coVerify(exactly = 1) { translationDataSource.translate(rakijaEnglish, englishLanguageCode, serbianLanguageCode) }
        verify(exactly = 1) { dictionaryDataSource.setLoadingTranslation(rakijaEnglish, serbianLanguageCode) }
        verify(exactly = 0) { dictionaryDataSource.removeTranslation(rakijaEnglish, serbianLanguageCode) }
        assertEquals(null, response)

        // When
        advanceTimeBy(repeatIntervalOnError)
        runCurrent()

        // Then
        verify(exactly = 1) { dictionaryDataSource.removeTranslation(rakijaEnglish, serbianLanguageCode) }
        verify(exactly = 0) { dictionaryDataSource.putTranslation(rakijaEnglish, serbianLanguageCode, rakijaSerbian) }
    }

    @Test
    fun `test getTranslation called 2 times after more than 1s, translate throws exception both times, null returned and translation removed after 1s both times`() = runTest {
        // Given
        every { dictionaryDataSource.getTranslation(any(), any()) } returns null
        coEvery { translationDataSource.translate(any(), any(), any()) }.returns(
            Either.Error(Exception())
        )

        // When
        val response = sut.getTranslation(rakijaEnglish, serbianLanguageCode)

        // Then
        verify(exactly = 1) { dictionaryDataSource.getTranslation(rakijaEnglish, serbianLanguageCode) }
        coVerify(exactly = 1) { translationDataSource.translate(rakijaEnglish, englishLanguageCode, serbianLanguageCode) }
        verify(exactly = 1) { dictionaryDataSource.setLoadingTranslation(rakijaEnglish, serbianLanguageCode) }
        verify(exactly = 0) { dictionaryDataSource.removeTranslation(rakijaEnglish, serbianLanguageCode) }
        assertEquals(null, response)

        // When
        advanceTimeBy(repeatIntervalOnError)
        runCurrent()

        // Then
        verify(exactly = 1) { dictionaryDataSource.removeTranslation(rakijaEnglish, serbianLanguageCode) }

        // When
        val secondResponse = sut.getTranslation(rakijaEnglish, serbianLanguageCode)

        // Then
        verify(exactly = 2) { dictionaryDataSource.getTranslation(rakijaEnglish, serbianLanguageCode) }
        coVerify(exactly = 2) { translationDataSource.translate(rakijaEnglish, englishLanguageCode, serbianLanguageCode) }
        verify(exactly = 2) { dictionaryDataSource.setLoadingTranslation(rakijaEnglish, serbianLanguageCode) }
        verify(exactly = 1) { dictionaryDataSource.removeTranslation(rakijaEnglish, serbianLanguageCode) }
        assertEquals(null, secondResponse)

        // When
        advanceTimeBy(repeatIntervalOnError)
        runCurrent()

        // Then
        verify(exactly = 2) { dictionaryDataSource.removeTranslation(rakijaEnglish, serbianLanguageCode) }
        verify(exactly = 0) { dictionaryDataSource.putTranslation(rakijaEnglish, serbianLanguageCode, rakijaSerbian) }
    }

    @Test
    fun `test getTranslation, translate has response, null returned and new translation stored`() = runTest {
        // Given
        every { dictionaryDataSource.getTranslation(any(), any()) } returns null
        coEvery { translationDataSource.translate(any(), any(), any()) }.returns(
            Either.Result(rakijaSerbian)
        )

        // When
        val response = sut.getTranslation(rakijaEnglish, serbianLanguageCode)
        advanceUntilIdle()
        runCurrent()

        // Then
        verify(exactly = 1) { dictionaryDataSource.getTranslation(rakijaEnglish, serbianLanguageCode) }
        verify(exactly = 1) { dictionaryDataSource.setLoadingTranslation(rakijaEnglish, serbianLanguageCode) }
        coVerify(exactly = 1) { translationDataSource.translate(rakijaEnglish, englishLanguageCode, serbianLanguageCode) }
        verify(exactly = 1) { dictionaryDataSource.putTranslation(rakijaEnglish, serbianLanguageCode, rakijaSerbian) }
        verify(exactly = 0) { dictionaryDataSource.removeTranslation(rakijaEnglish, serbianLanguageCode) }
        assertEquals(null, response)
    }

    @Test
    fun `test getTranslation called 2 times, second call was executed before there was response from the first call, new translation stored and null returned`() = runTest {
        // Given
        every { dictionaryDataSource.getTranslation(any(), any()) } returnsMany listOf(null, TranslationState.Loading)
        coEvery { translationDataSource.translate(any(), any(), any()) }.returns(
            Either.Result(rakijaSerbian)
        )

        // When
        sut.getTranslation(rakijaEnglish, serbianLanguageCode)
        val response = sut.getTranslation(rakijaEnglish, serbianLanguageCode)

        advanceUntilIdle()
        runCurrent()

        // Then
        verify(exactly = 2) { dictionaryDataSource.getTranslation(rakijaEnglish, serbianLanguageCode) }
        verify(exactly = 1) { dictionaryDataSource.setLoadingTranslation(rakijaEnglish, serbianLanguageCode) }
        coVerify(exactly = 1) { translationDataSource.translate(rakijaEnglish, englishLanguageCode, serbianLanguageCode) }
        verify(exactly = 1) { dictionaryDataSource.putTranslation(rakijaEnglish, serbianLanguageCode, rakijaSerbian) }
        verify(exactly = 0) { dictionaryDataSource.removeTranslation(rakijaEnglish, serbianLanguageCode) }
        assertEquals(null, response)
    }

    @Test
    fun `test getTranslation called 2 times, second call was executed after there was error from the first call after 1s, translation stored and null returned`() = runTest {
        // Given
        every { dictionaryDataSource.getTranslation(any(), any()) } returns null
        coEvery { translationDataSource.translate(any(), any(), any()) }.returnsMany(
            Either.Error(Exception()),
            Either.Result(rakijaSerbian)
        )

        // When
        val response = sut.getTranslation(rakijaEnglish, serbianLanguageCode)

        // Then
        verify(exactly = 1) { dictionaryDataSource.getTranslation(rakijaEnglish, serbianLanguageCode) }
        coVerify(exactly = 1) { translationDataSource.translate(rakijaEnglish, englishLanguageCode, serbianLanguageCode) }
        verify(exactly = 1) { dictionaryDataSource.setLoadingTranslation(rakijaEnglish, serbianLanguageCode) }
        verify(exactly = 0) { dictionaryDataSource.putTranslation(rakijaEnglish, serbianLanguageCode, rakijaSerbian) }
        verify(exactly = 0) { dictionaryDataSource.removeTranslation(rakijaEnglish, serbianLanguageCode) }
        assertEquals(null, response)

        // When
        advanceTimeBy(repeatIntervalOnError)
        runCurrent()

        // Then
        verify(exactly = 1) { dictionaryDataSource.removeTranslation(rakijaEnglish, serbianLanguageCode) }

        // When
        val secondResponse = sut.getTranslation(rakijaEnglish, serbianLanguageCode)

        advanceUntilIdle()
        runCurrent()

        // Then
        verify(exactly = 2) { dictionaryDataSource.getTranslation(rakijaEnglish, serbianLanguageCode) }
        coVerify(exactly = 2) { translationDataSource.translate(rakijaEnglish, englishLanguageCode, serbianLanguageCode) }
        verify(exactly = 2) { dictionaryDataSource.setLoadingTranslation(rakijaEnglish, serbianLanguageCode) }
        verify(exactly = 1) { dictionaryDataSource.putTranslation(rakijaEnglish, serbianLanguageCode, rakijaSerbian) }
        verify(exactly = 1) { dictionaryDataSource.removeTranslation(rakijaEnglish, serbianLanguageCode) }
        assertEquals(null, secondResponse)
    }

    @Test
    fun `test getTranslation called 2 times, second call was executed after there was response from the first call, translation stored and translation returned the second call`() = runTest {
        // Given
        every { dictionaryDataSource.getTranslation(any(), any()) }.returnsMany(
            null,
            TranslationState.Value(rakijaSerbian)
        )
        coEvery { translationDataSource.translate(any(), any(), any()) }.returns(
            Either.Result(rakijaSerbian)
        )

        // When
        val response = sut.getTranslation(rakijaEnglish, serbianLanguageCode)

        advanceUntilIdle()
        runCurrent()

        // Then
        verify(exactly = 1) { dictionaryDataSource.getTranslation(rakijaEnglish, serbianLanguageCode) }
        coVerify(exactly = 1) { translationDataSource.translate(rakijaEnglish, englishLanguageCode, serbianLanguageCode) }
        verify(exactly = 1) { dictionaryDataSource.setLoadingTranslation(rakijaEnglish, serbianLanguageCode) }
        verify(exactly = 1) { dictionaryDataSource.putTranslation(rakijaEnglish, serbianLanguageCode, rakijaSerbian) }
        verify(exactly = 0) { dictionaryDataSource.removeTranslation(rakijaEnglish, serbianLanguageCode) }
        assertEquals(null, response)

        // When
        val secondResponse = sut.getTranslation(rakijaEnglish, serbianLanguageCode)

        advanceUntilIdle()
        runCurrent()

        // Then
        verify(exactly = 2) { dictionaryDataSource.getTranslation(rakijaEnglish, serbianLanguageCode) }
        coVerify(exactly = 1) { translationDataSource.translate(rakijaEnglish, englishLanguageCode, serbianLanguageCode) }
        verify(exactly = 1) { dictionaryDataSource.setLoadingTranslation(rakijaEnglish, serbianLanguageCode) }
        verify(exactly = 1) { dictionaryDataSource.putTranslation(rakijaEnglish, serbianLanguageCode, rakijaSerbian) }
        verify(exactly = 0) { dictionaryDataSource.removeTranslation(rakijaEnglish, serbianLanguageCode) }
        assertEquals(rakijaSerbian, secondResponse)
    }
}
