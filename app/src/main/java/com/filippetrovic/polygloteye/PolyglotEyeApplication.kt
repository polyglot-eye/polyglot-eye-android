package com.filippetrovic.polygloteye

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PolyglotEyeApplication : Application()
