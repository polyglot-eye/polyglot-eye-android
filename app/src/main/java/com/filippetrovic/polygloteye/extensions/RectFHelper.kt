package com.filippetrovic.polygloteye.extensions

import android.graphics.RectF

fun RectF.area(): Float {
    return width() * height()
}
