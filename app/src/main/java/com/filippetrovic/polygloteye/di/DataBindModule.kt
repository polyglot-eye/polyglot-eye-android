package com.filippetrovic.polygloteye.di

import com.filippetrovic.polygloteye.data.datasource.dictionary.DictionaryDataSource
import com.filippetrovic.polygloteye.data.datasource.dictionary.IDictionaryDataSource
import com.filippetrovic.polygloteye.data.datasource.translation.GoogleTranslationDataSource
import com.filippetrovic.polygloteye.data.datasource.translation.ITranslationDataSource
import com.filippetrovic.polygloteye.data.manager.detection.IObjectDetectorManager
import com.filippetrovic.polygloteye.data.manager.detection.TensorflowObjectDetectorManager
import com.filippetrovic.polygloteye.data.player.IPlayer
import com.filippetrovic.polygloteye.data.player.PolyglotEyePlayer
import com.filippetrovic.polygloteye.data.repository.dictionary.DictionaryRepository
import com.filippetrovic.polygloteye.data.repository.dictionary.IDictionaryRepository
import com.filippetrovic.polygloteye.data.repository.texttospeech.GoogleTextToSpeechRepository
import com.filippetrovic.polygloteye.data.repository.texttospeech.ITextToSpeechRepository
import com.filippetrovic.polygloteye.data.repository.userpreferences.IUserPreferencesRepository
import com.filippetrovic.polygloteye.data.repository.userpreferences.UserPreferencesRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class DataBindModule {

    @Singleton
    @Binds
    abstract fun bindDictionaryDataSource(dictionaryDataSource: DictionaryDataSource): IDictionaryDataSource

    @Singleton
    @Binds
    abstract fun bindGoogleTranslationDataSource(translationService: GoogleTranslationDataSource): ITranslationDataSource

    @Singleton
    @Binds
    abstract fun bindUserPreferencesRepository(
        repo: UserPreferencesRepository
    ): IUserPreferencesRepository

    @Singleton
    @Binds
    abstract fun bindDictionaryRepository(repo: DictionaryRepository): IDictionaryRepository

    @Singleton
    @Binds
    abstract fun bindTextToSpeechRepository(repo: GoogleTextToSpeechRepository): ITextToSpeechRepository

    @Singleton
    @Binds
    abstract fun bindTensorflowObjectDetectorManager(
        manager: TensorflowObjectDetectorManager
    ): IObjectDetectorManager

    @Singleton
    @Binds
    abstract fun bindPolyglotEyePlayer(
        player: PolyglotEyePlayer
    ): IPlayer
}
