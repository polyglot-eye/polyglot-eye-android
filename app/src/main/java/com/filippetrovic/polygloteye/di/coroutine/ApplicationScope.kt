package com.filippetrovic.polygloteye.di.coroutine

import javax.inject.Qualifier

@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class ApplicationScope
