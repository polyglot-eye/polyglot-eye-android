package com.filippetrovic.polygloteye.di

import android.app.Application
import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.core.DataStoreFactory
import androidx.datastore.dataStoreFile
import com.filippetrovic.polygloteye.R
import com.filippetrovic.polygloteye.data.datastore.UserPreferencesSerializer
import com.filippetrovic.polygloteye.data.datastore.model.UserPreferences
import com.google.api.gax.core.FixedCredentialsProvider
import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.texttospeech.v1.TextToSpeechClient
import com.google.cloud.texttospeech.v1.TextToSpeechSettings
import com.google.cloud.translate.Translate
import com.google.cloud.translate.TranslateOptions
import com.google.crypto.tink.Aead
import com.google.crypto.tink.KeyTemplates
import com.google.crypto.tink.aead.AeadConfig
import com.google.crypto.tink.integration.android.AndroidKeysetManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import java.io.InputStream
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataModule {

    companion object {
        private const val DATA_STORE_FILE_NAME = "user_preferences.pb"
        private const val KEYSET_NAME = "master_keyset"
        private const val PREFERENCE_FILE = "master_key_preference"
        private const val MASTER_KEY_URI = "android-keystore://master_key"
    }

    @Provides
    @Singleton
    fun provideGoogleTranslationService(
        @ApplicationContext context: Context
    ): Translate {
        val translationCredentialsInputStream = context.resources.openRawResource(R.raw.translation_credentials)
        val translationCredentials = GoogleCredentials.fromStream(translationCredentialsInputStream)
        val translateOptions = TranslateOptions.newBuilder().setCredentials(translationCredentials).build()

        return translateOptions.service
    }

    @Provides
    @Singleton
    fun provideGoogleTextToSpeechClient(
        @ApplicationContext context: Context
    ): TextToSpeechClient {
        val stream: InputStream = context.resources.openRawResource(R.raw.text_to_speech_credentials)
        val credentials: GoogleCredentials = GoogleCredentials.fromStream(stream)
        val sessionsSettings = TextToSpeechSettings.newBuilder()
            .setCredentialsProvider(FixedCredentialsProvider.create(credentials))
            .build()

        return TextToSpeechClient.create(sessionsSettings)
    }

    @Singleton
    @Provides
    fun provideAead(application: Application): Aead {
        AeadConfig.register()

        return AndroidKeysetManager.Builder()
            .withSharedPref(application, KEYSET_NAME, PREFERENCE_FILE)
            .withKeyTemplate(KeyTemplates.get("AES256_GCM"))
            .withMasterKeyUri(MASTER_KEY_URI)
            .build()
            .keysetHandle
            .getPrimitive(Aead::class.java)
    }

    @Singleton
    @Provides
    fun provideUserPreferencesDataStore(
        @ApplicationContext appContext: Context,
        userPreferencesSerializer: UserPreferencesSerializer
    ): DataStore<UserPreferences> {
        return DataStoreFactory.create(
            serializer = userPreferencesSerializer,
            produceFile = { appContext.dataStoreFile(DATA_STORE_FILE_NAME) },
            corruptionHandler = null,
            migrations = listOf(),
            scope = CoroutineScope(Dispatchers.IO + SupervisorJob())
        )
    }
}
