package com.filippetrovic.polygloteye.di.coroutine

import javax.inject.Qualifier

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class IoDispatcher
