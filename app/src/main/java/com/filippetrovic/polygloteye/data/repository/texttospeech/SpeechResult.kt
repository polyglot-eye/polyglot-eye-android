package com.filippetrovic.polygloteye.data.repository.texttospeech

data class SpeechResult(
    val audio: ByteArray?,
    val sampleRate: Int
)
