package com.filippetrovic.polygloteye.data.repository.userpreferences

import androidx.datastore.core.DataStore
import com.filippetrovic.polygloteye.data.datasource.translation.model.LanguageModel
import com.filippetrovic.polygloteye.data.datastore.model.UserPreferences
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class UserPreferencesRepository @Inject constructor(
    private val dataStore: DataStore<UserPreferences>
) : IUserPreferencesRepository {

    override suspend fun getSourceLanguage(): Flow<LanguageModel?> {
        return getValues { it.sourceLanguage }
    }

    override suspend fun updateSourceLanguage(value: LanguageModel) {
        dataStore.updateData { it.copy(sourceLanguage = value) }
    }

    override suspend fun removeSourceLanguage() {
        dataStore.updateData { it.copy(sourceLanguage = null) }
    }

    override suspend fun getTargetLanguage(): Flow<LanguageModel?> {
        return getValues { it.targetLanguage }
    }

    override suspend fun updateTargetLanguage(value: LanguageModel) {
        dataStore.updateData { it.copy(targetLanguage = value) }
    }

    override suspend fun removeTargetLanguage() {
        dataStore.updateData { it.copy(targetLanguage = null) }
    }

    // region Private API

    private fun <T> getValues(block: (UserPreferences) -> T): Flow<T> {
        return dataStore.data.map {
            block(it)
        }
    }

    // endregion
}
