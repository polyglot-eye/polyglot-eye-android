package com.filippetrovic.polygloteye.data.repository.userpreferences

import com.filippetrovic.polygloteye.data.datasource.translation.model.LanguageModel
import kotlinx.coroutines.flow.Flow

interface IUserPreferencesRepository {

    suspend fun getSourceLanguage(): Flow<LanguageModel?>
    suspend fun updateSourceLanguage(value: LanguageModel)
    suspend fun removeSourceLanguage()

    suspend fun getTargetLanguage(): Flow<LanguageModel?>
    suspend fun updateTargetLanguage(value: LanguageModel)
    suspend fun removeTargetLanguage()
}
