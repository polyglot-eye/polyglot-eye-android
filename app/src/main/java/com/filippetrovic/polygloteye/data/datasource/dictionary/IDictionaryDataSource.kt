package com.filippetrovic.polygloteye.data.datasource.dictionary

interface IDictionaryDataSource {
    fun setLoadingTranslation(identifier: String, languageCode: String)
    fun putTranslation(identifier: String, languageCode: String, translation: String)
    fun getTranslation(identifier: String, languageCode: String): TranslationState?
    fun removeTranslation(identifier: String, languageCode: String)
}
