package com.filippetrovic.polygloteye.data.datasource.texttospeech

import com.filippetrovic.polygloteye.functional.Either
import com.filippetrovic.polygloteye.functional.runCatchingEither
import com.google.cloud.texttospeech.v1.AudioConfig
import com.google.cloud.texttospeech.v1.SynthesisInput
import com.google.cloud.texttospeech.v1.SynthesizeSpeechResponse
import com.google.cloud.texttospeech.v1.TextToSpeechClient
import com.google.cloud.texttospeech.v1.Voice
import com.google.cloud.texttospeech.v1.VoiceSelectionParams
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GoogleTextToSpeechDataSource @Inject constructor(
    private val textToSpeechClient: TextToSpeechClient
) {

    fun synthesize(
        synthesisInput: SynthesisInput,
        voiceSelectionParams: VoiceSelectionParams,
        audioConfig: AudioConfig
    ): Either<Exception, SynthesizeSpeechResponse> {
        return runCatchingEither {
            textToSpeechClient.synthesizeSpeech(
                synthesisInput,
                voiceSelectionParams,
                audioConfig
            )
        }
    }

    fun getVoices(languageCode: String): Either<Exception, List<Voice>?> {
        return runCatchingEither {
            textToSpeechClient.listVoices(languageCode)?.voicesList
        }
    }

    fun getNativeLanguageCode(languageCode: String): String {
        return when (languageCode) {
            "nl" -> "nl-NL"
            "en" -> "en-US"
            "fr" -> "fr-FR"
            "pt" -> "pt-PT"
            "es" -> "es-ES"
            else -> languageCode
        }
    }
}
