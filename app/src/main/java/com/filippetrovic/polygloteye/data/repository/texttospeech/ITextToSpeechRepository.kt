package com.filippetrovic.polygloteye.data.repository.texttospeech

import com.filippetrovic.polygloteye.functional.Either

interface ITextToSpeechRepository {

    suspend fun synthesize(text: String, languageCode: String): Either<Exception, SpeechResult>
}
