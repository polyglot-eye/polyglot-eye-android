package com.filippetrovic.polygloteye.data.repository.texttospeech

import com.google.cloud.texttospeech.v1.Voice
import com.google.cloud.texttospeech.v1.VoiceSelectionParams
import javax.inject.Inject

class VoiceBuilder @Inject constructor() {

    fun build(voice: Voice): VoiceSelectionParams {
        return VoiceSelectionParams.newBuilder()
            .setName(voice.name)
            .setLanguageCode(voice.languageCodesList.first())
            .build()
    }
}
