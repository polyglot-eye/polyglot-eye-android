package com.filippetrovic.polygloteye.data.repository.dictionary

import com.filippetrovic.polygloteye.data.datasource.dictionary.IDictionaryDataSource
import com.filippetrovic.polygloteye.data.datasource.translation.ITranslationDataSource
import com.filippetrovic.polygloteye.data.datasource.translation.model.LanguageModel
import com.filippetrovic.polygloteye.di.coroutine.ApplicationScope
import com.filippetrovic.polygloteye.di.coroutine.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class DictionaryRepository @Inject constructor(
    @ApplicationScope private val scope: CoroutineScope,
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val dictionaryDataSource: IDictionaryDataSource,
    private val translationDataSource: ITranslationDataSource
) : IDictionaryRepository {

    // region Companion

    private companion object {
        const val REPEAT_INTERVAL_ON_ERROR = 1000L
    }

    // endregion

    // region Public API

    override fun getTranslation(word: String, languageCode: String): String? {
        val localTranslation = dictionaryDataSource.getTranslation(word, languageCode)

        if (localTranslation == null) fetchTranslation(word, languageCode)

        return localTranslation?.getText()
    }

    // endregion

    // region Private API

    private fun fetchTranslation(word: String, languageCode: String) {
        dictionaryDataSource.setLoadingTranslation(word, languageCode)

        scope.launch(dispatcher) {
            translationDataSource.translate(
                word,
                LanguageModel.getDefaultLanguageModel().code,
                languageCode
            )
                .onResult { handleServerTranslationResult(word, languageCode, it) }
                .onError { handleServerTranslationError(word, languageCode, it) }
        }
    }

    private fun handleServerTranslationResult(
        word: String,
        languageCode: String,
        translation: String
    ) {
        dictionaryDataSource.putTranslation(word, languageCode, translation)
    }

    private fun handleServerTranslationError(
        word: String,
        languageCode: String,
        exception: Exception
    ) {
        scope.launch(dispatcher) {
            delay(REPEAT_INTERVAL_ON_ERROR)
            dictionaryDataSource.removeTranslation(word, languageCode)
        }
    }

    // endregion
}
