package com.filippetrovic.polygloteye.data.repository.texttospeech

import com.google.cloud.texttospeech.v1.SynthesisInput
import javax.inject.Inject

class SynthesisInputBuilder @Inject constructor() {

    fun build(text: String): SynthesisInput {
        return SynthesisInput.newBuilder()
            .setText(text)
            .build()
    }
}
