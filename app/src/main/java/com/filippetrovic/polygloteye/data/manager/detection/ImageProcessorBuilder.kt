package com.filippetrovic.polygloteye.data.manager.detection

import org.tensorflow.lite.support.image.ImageProcessor
import org.tensorflow.lite.support.image.ops.Rot90Op
import javax.inject.Inject

class ImageProcessorBuilder @Inject constructor() {

    fun build(imageRotation: Int): ImageProcessor =
        ImageProcessor.Builder()
            .add(Rot90Op(-imageRotation / 90))
            .build()
}
