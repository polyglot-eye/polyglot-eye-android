package com.filippetrovic.polygloteye.data.repository.texttospeech

import com.filippetrovic.polygloteye.data.datasource.texttospeech.GoogleTextToSpeechDataSource
import com.filippetrovic.polygloteye.data.datasource.texttospeech.GoogleVoiceType.STANDARD
import com.filippetrovic.polygloteye.functional.Either
import com.google.android.gms.common.api.ApiException
import com.google.cloud.texttospeech.v1.AudioConfig
import com.google.cloud.texttospeech.v1.AudioEncoding
import io.grpc.StatusRuntimeException
import javax.inject.Inject

class GoogleTextToSpeechRepository @Inject constructor(
    private val dataSource: GoogleTextToSpeechDataSource,
    private val voiceBuilder: VoiceBuilder,
    private val synthesisInputBuilder: SynthesisInputBuilder
) : ITextToSpeechRepository {

    override suspend fun synthesize(
        text: String,
        languageCode: String
    ): Either<Exception, SpeechResult> =

        try {
            val nativeLanguageCode = dataSource.getNativeLanguageCode(languageCode)
            val voiceResponse = dataSource.getVoices(nativeLanguageCode)

            if (voiceResponse.isError) throw voiceResponse.error()

            val voice = voiceResponse.result()?.find { it.name.contains(STANDARD.voiceName) }

            if (voice == null) {
                Either.Error(Exception("Language not supported"))
            } else {
                val voiceSelectionParams = voiceBuilder.build(voice)

                val audioConfig = AudioConfig.newBuilder()
                    .setAudioEncoding(AudioEncoding.LINEAR16)
                    .build()

                val synthesisInput = synthesisInputBuilder.build(text)

                val synthesizeSpeechResponse = dataSource.synthesize(
                    synthesisInput,
                    voiceSelectionParams,
                    audioConfig
                )

                if (synthesizeSpeechResponse.isError) throw synthesizeSpeechResponse.error()

                val synthesizedSpeech = synthesizeSpeechResponse.result()

                Either.Result(
                    SpeechResult(
                        synthesizedSpeech.audioContent.toByteArray(),
                        voice.naturalSampleRateHertz
                    )
                )
            }
        } catch (e: ApiException) {
            Either.Error(e)
        } catch (e: StatusRuntimeException) {
            Either.Error(e)
        } catch (e: Exception) {
            Either.Error(e)
        }
}
