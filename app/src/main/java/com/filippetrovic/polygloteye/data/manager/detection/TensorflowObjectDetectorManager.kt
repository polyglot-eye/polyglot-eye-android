package com.filippetrovic.polygloteye.data.manager.detection

import android.content.Context
import android.graphics.Bitmap
import android.os.SystemClock
import com.filippetrovic.polygloteye.R
import com.filippetrovic.polygloteye.data.manager.detection.DelegateType.CPU
import com.filippetrovic.polygloteye.data.manager.detection.DelegateType.GPU
import com.filippetrovic.polygloteye.data.manager.detection.DelegateType.NNAPI
import com.filippetrovic.polygloteye.functional.Either
import dagger.hilt.android.qualifiers.ApplicationContext
import org.tensorflow.lite.gpu.CompatibilityList
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.task.core.BaseOptions
import org.tensorflow.lite.task.vision.detector.ObjectDetector
import javax.inject.Inject

class TensorflowObjectDetectorManager @Inject constructor(
    @ApplicationContext private val context: Context,
    private val imageProcessorBuilder: ImageProcessorBuilder
) : IObjectDetectorManager {

    // region Companion object

    companion object {
        const val MODEL_NAME = "object_detection.tflite"
    }

    // endregion

    // region Properties

    private var threshold = 0.5f
    private var numThreads = 2
    private var maxResults = 3
    private var currentDelegate = CPU
    private lateinit var objectDetector: ObjectDetector

    // endregion

    // region Public API

    override fun detect(image: Bitmap, imageRotation: Int): Either<Exception, DetectionResult> {
        if (::objectDetector.isInitialized.not()) {
            val initializationResponse = initialize()
            if (initializationResponse is Either.Error) return initializationResponse
        }

        var inferenceTime = SystemClock.uptimeMillis()

        val imageProcessor = imageProcessorBuilder.build(imageRotation)

        val tensorImage = imageProcessor.process(TensorImage.fromBitmap(image))
        val detections = objectDetector.detect(tensorImage)
        inferenceTime = SystemClock.uptimeMillis() - inferenceTime

        return Either.Result(
            DetectionResult(
                detections = detections?.filterNotNull() ?: emptyList(),
                inferenceTime = inferenceTime,
                imageHeight = tensorImage.height,
                imageWidth = tensorImage.width
            )
        )
    }

    override fun initialize(): Either<Exception, Unit> {
        val optionsBuilder =
            ObjectDetector.ObjectDetectorOptions.builder()
                .setScoreThreshold(threshold)
                .setMaxResults(maxResults)

        val baseOptionsBuilder = BaseOptions.builder().setNumThreads(numThreads)

        when (currentDelegate) {
            CPU -> {}
            GPU -> {
                if (CompatibilityList().isDelegateSupportedOnThisDevice) {
                    baseOptionsBuilder.useGpu()
                } else {
                    return Either.Error(Exception(context.getString(R.string.gpu_not_supported_error_message)))
                }
            }
            NNAPI -> {
                baseOptionsBuilder.useNnapi()
            }
        }

        optionsBuilder.setBaseOptions(baseOptionsBuilder.build())

        try {
            objectDetector =
                ObjectDetector.createFromFileAndOptions(context, MODEL_NAME, optionsBuilder.build())
        } catch (e: IllegalStateException) {
            return Either.Error(Exception(context.getString(R.string.object_detector_initialization_failure_error_message)))
        }

        return Either.Result(Unit)
    }

    // endregion
}
