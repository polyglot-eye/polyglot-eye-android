package com.filippetrovic.polygloteye.data.manager.detection

import org.tensorflow.lite.task.vision.detector.Detection

data class DetectionResult(
    val detections: List<Detection> = emptyList(),
    val inferenceTime: Long = 0L,
    val imageHeight: Int = 0,
    val imageWidth: Int = 0
)
