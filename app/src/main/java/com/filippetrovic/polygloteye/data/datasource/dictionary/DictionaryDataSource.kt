package com.filippetrovic.polygloteye.data.datasource.dictionary

import com.filippetrovic.polygloteye.data.datasource.dictionary.TranslationState.Loading
import com.filippetrovic.polygloteye.data.datasource.dictionary.TranslationState.Value
import javax.inject.Inject

class DictionaryDataSource @Inject constructor() : IDictionaryDataSource {

    // Each identifier has [languageCode to value] translation pairs
    private val dictionary:
        MutableMap<String, MutableMap<String, TranslationState>> = mutableMapOf()

    override fun setLoadingTranslation(identifier: String, languageCode: String) {
        val foundWord = dictionary.getOrPut(identifier) { mutableMapOf() }
        foundWord[languageCode] = Loading
    }

    override fun putTranslation(identifier: String, languageCode: String, translation: String) {
        val foundWord = dictionary.getOrPut(identifier) { mutableMapOf() }
        foundWord[languageCode] = Value(translation)
    }

    override fun getTranslation(identifier: String, languageCode: String): TranslationState? {
        val foundWord = dictionary[identifier]
        return foundWord?.get(languageCode)
    }

    override fun removeTranslation(identifier: String, languageCode: String) {
        val foundWord = dictionary.getOrPut(identifier) { mutableMapOf() }
        foundWord.remove(languageCode)
    }
}
