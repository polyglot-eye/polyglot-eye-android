package com.filippetrovic.polygloteye.data.manager.detection

enum class DelegateType {
    CPU,
    GPU,
    NNAPI
}
