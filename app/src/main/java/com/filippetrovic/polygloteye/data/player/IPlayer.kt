package com.filippetrovic.polygloteye.data.player

interface IPlayer {
    var sampleRate: Int
    var isPlaying: Boolean

    suspend fun play(audio: ByteArray)
    fun stop()
}
