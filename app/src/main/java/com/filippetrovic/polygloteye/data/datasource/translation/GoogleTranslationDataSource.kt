package com.filippetrovic.polygloteye.data.datasource.translation

import com.filippetrovic.polygloteye.data.datasource.translation.model.LanguageModel
import com.filippetrovic.polygloteye.di.coroutine.IoDispatcher
import com.filippetrovic.polygloteye.functional.Either
import com.filippetrovic.polygloteye.functional.runCatchingEither
import com.google.cloud.translate.Translate
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GoogleTranslationDataSource @Inject constructor(
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val translationService: Translate
) : ITranslationDataSource {

    override suspend fun translate(
        text: String,
        sourceLanguage: String,
        targetLanguage: String
    ): Either<Exception, String> = withContext(dispatcher) {
        if (sourceLanguage == targetLanguage) return@withContext Either.Result(text)

        return@withContext runCatchingEither {
            translationService.translate(
                text,
                Translate.TranslateOption.sourceLanguage(sourceLanguage),
                Translate.TranslateOption.targetLanguage(targetLanguage)
            ).translatedText
        }
    }

    override suspend fun getSupportedLanguages(): Either<Exception, List<LanguageModel>> = withContext(dispatcher) {
        return@withContext runCatchingEither {
            translationService.listSupportedLanguages().distinctBy { it.name }.map {
                LanguageModel(it.code, it.name)
            }
        }
    }
}
