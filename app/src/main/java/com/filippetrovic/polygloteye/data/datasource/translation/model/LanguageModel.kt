package com.filippetrovic.polygloteye.data.datasource.translation.model

import kotlinx.serialization.Serializable

@Serializable
data class LanguageModel(
    val code: String,
    val name: String
) {
    companion object {
        fun getDefaultLanguageModel() = LanguageModel("en", "English")
    }
}
