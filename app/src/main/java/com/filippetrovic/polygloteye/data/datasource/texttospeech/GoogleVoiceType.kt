package com.filippetrovic.polygloteye.data.datasource.texttospeech

enum class GoogleVoiceType(val voiceName: String) {
    STANDARD("Standard"),
    STUDIO("Studio"),
    NEURAL2("Neural2"),
    WAVE_NET("WaveNet")
}
