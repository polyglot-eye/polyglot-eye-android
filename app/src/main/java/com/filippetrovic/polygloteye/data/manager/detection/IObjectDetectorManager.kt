package com.filippetrovic.polygloteye.data.manager.detection

import android.graphics.Bitmap
import com.filippetrovic.polygloteye.functional.Either

interface IObjectDetectorManager {
    fun initialize(): Either<Exception, Unit>
    fun detect(image: Bitmap, imageRotation: Int): Either<Exception, DetectionResult>
}
