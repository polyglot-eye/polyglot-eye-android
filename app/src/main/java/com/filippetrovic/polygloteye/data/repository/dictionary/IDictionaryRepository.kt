package com.filippetrovic.polygloteye.data.repository.dictionary

interface IDictionaryRepository {
    fun getTranslation(word: String, languageCode: String): String?
}
