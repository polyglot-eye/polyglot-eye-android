package com.filippetrovic.polygloteye.data.datastore.model

import com.filippetrovic.polygloteye.data.datasource.translation.model.LanguageModel
import kotlinx.serialization.Serializable

@Serializable
data class UserPreferences(
    val sourceLanguage: LanguageModel? = null,
    val targetLanguage: LanguageModel? = null
)
