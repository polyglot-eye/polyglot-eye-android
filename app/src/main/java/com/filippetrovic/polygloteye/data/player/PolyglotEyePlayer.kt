package com.filippetrovic.polygloteye.data.player

import android.media.AudioAttributes
import android.media.AudioFormat
import android.media.AudioTrack
import com.filippetrovic.polygloteye.di.coroutine.ApplicationScope
import com.filippetrovic.polygloteye.di.coroutine.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import javax.inject.Inject

class PolyglotEyePlayer @Inject constructor(
    @ApplicationScope private val scope: CoroutineScope,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : IPlayer {

    // region Companion

    companion object {
        const val HEADER_SIZE_IN_BYTES = 44
    }

    // endregion

    // region Properties

    override var sampleRate: Int = 24000
        set(value) {
            audioTrack.playbackRate = value
            field = value
        }

    override var isPlaying: Boolean = false

    private lateinit var audioTrack: AudioTrack
    private val mutex = Mutex()

    // endregion

    // region Init

    init {
        setUp()
    }

    // endregion

    // endregion

    // region Public API

    override suspend fun play(audio: ByteArray) {
        if (::audioTrack.isInitialized.not() || audio.size <= HEADER_SIZE_IN_BYTES) return

        // Wait for the previous executions to start playing in order to stop them properly
        mutex.lock()

        if (isPlaying) stop()

        // Launch in a coroutine because audioTrack.write() blocks the thread
        scope.launch(dispatcher) {
            internalPlay(audio)
        }
    }

    override fun stop() {
        audioTrack.pause()
        audioTrack.flush()
    }

    // endregion

    // region Private API

    // This function is synchronized because audioTrack.stop() must execute before the new audioTrack.play(), to prevent accidentally stopping the new execution
    private fun internalPlay(audio: ByteArray) = synchronized(this) {
        isPlaying = true
        audioTrack.play()

        // Playing started, so now it can be stopped by new executions
        mutex.unlock()

        val audioToPlay = audio.copyOfRange(HEADER_SIZE_IN_BYTES, audio.size - 1)

        audioTrack.write(audioToPlay, 0, audioToPlay.size)
        audioTrack.stop()
        isPlaying = false
    }

    private fun setUp() {
        val channelConfig = AudioFormat.CHANNEL_OUT_MONO
        val audioFormat = AudioFormat.ENCODING_PCM_16BIT

        val bufferSize = AudioTrack.getMinBufferSize(
            sampleRate,
            channelConfig,
            audioFormat
        )

        audioTrack = AudioTrack.Builder()
            .setAudioAttributes(
                AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build()
            )
            .setAudioFormat(
                AudioFormat.Builder()
                    .setEncoding(audioFormat)
                    .setSampleRate(sampleRate)
                    .setChannelMask(channelConfig)
                    .build()
            )
            .setBufferSizeInBytes(bufferSize)
            .setTransferMode(AudioTrack.MODE_STREAM)
            .build()
    }

    // endregion
}
