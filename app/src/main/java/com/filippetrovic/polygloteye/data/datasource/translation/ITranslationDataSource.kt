package com.filippetrovic.polygloteye.data.datasource.translation

import com.filippetrovic.polygloteye.data.datasource.translation.model.LanguageModel
import com.filippetrovic.polygloteye.functional.Either

interface ITranslationDataSource {

    suspend fun translate(text: String, sourceLanguage: String, targetLanguage: String): Either<Exception, String>
    suspend fun getSupportedLanguages(): Either<Exception, List<LanguageModel>>
}
