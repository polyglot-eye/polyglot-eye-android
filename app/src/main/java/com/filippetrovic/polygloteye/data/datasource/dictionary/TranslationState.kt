package com.filippetrovic.polygloteye.data.datasource.dictionary

sealed class TranslationState {

    object Loading : TranslationState()
    data class Value(val translationText: String) : TranslationState()

    fun getText(): String? {
        return when (this) {
            is Value -> translationText
            else -> null
        }
    }
}
