package com.filippetrovic.polygloteye.data.datastore

import androidx.datastore.core.Serializer
import com.filippetrovic.polygloteye.data.datastore.model.UserPreferences
import com.google.crypto.tink.Aead
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import java.io.InputStream
import java.io.OutputStream
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserPreferencesSerializer @Inject constructor(
    private val aead: Aead
) : Serializer<UserPreferences> {

    companion object {
        private const val associatedData = "com.filippetrovic.polygloteye"
    }

    override val defaultValue: UserPreferences
        get() = UserPreferences()

    override suspend fun readFrom(input: InputStream): UserPreferences {
        val decryptedBytes = aead.decrypt(input.readBytes(), associatedData.toByteArray())

        return try {
            Json.decodeFromString(
                deserializer = UserPreferences.serializer(),
                string = decryptedBytes.decodeToString()
            )
        } catch (e: SerializationException) {
            e.printStackTrace()
            defaultValue
        }
    }

    override suspend fun writeTo(t: UserPreferences, output: OutputStream) = run {
        val encryptedByteArray = Json.encodeToString(
            serializer = UserPreferences.serializer(),
            value = t
        ).encodeToByteArray()

        val encryptedData = aead.encrypt(encryptedByteArray, associatedData.toByteArray())

        output.write(encryptedData)
    }
}
