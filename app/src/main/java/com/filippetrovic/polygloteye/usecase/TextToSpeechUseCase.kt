package com.filippetrovic.polygloteye.usecase

import com.filippetrovic.polygloteye.data.player.IPlayer
import com.filippetrovic.polygloteye.data.repository.texttospeech.ITextToSpeechRepository
import com.filippetrovic.polygloteye.di.coroutine.IoDispatcher
import com.filippetrovic.polygloteye.functional.Either
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class TextToSpeechUseCase @Inject constructor(
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val textToSpeechRepository: ITextToSpeechRepository,
    private val player: IPlayer
) {

    suspend operator fun invoke(text: String, languageCode: String): Either<Exception, Unit> = withContext(dispatcher) {
        val response = textToSpeechRepository.synthesize(text, languageCode)

        if (response is Either.Error) return@withContext response

        val result = response.result()

        return@withContext result.audio?.let { audio ->
            player.sampleRate = result.sampleRate
            player.play(audio)

            Either.Result(Unit)
        } ?: Either.Error(java.lang.Exception("No audio data"))
    }
}
