package com.filippetrovic.polygloteye.viewmodel

import android.graphics.Bitmap
import androidx.camera.core.ImageProxy
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.filippetrovic.polygloteye.R
import com.filippetrovic.polygloteye.data.datasource.translation.ITranslationDataSource
import com.filippetrovic.polygloteye.data.datasource.translation.model.LanguageModel
import com.filippetrovic.polygloteye.data.manager.detection.DetectionResult
import com.filippetrovic.polygloteye.data.manager.detection.IObjectDetectorManager
import com.filippetrovic.polygloteye.data.repository.dictionary.IDictionaryRepository
import com.filippetrovic.polygloteye.data.repository.userpreferences.IUserPreferencesRepository
import com.filippetrovic.polygloteye.functional.Consumable
import com.filippetrovic.polygloteye.functional.StringHolder
import com.filippetrovic.polygloteye.ui.objectdetection.model.DetectionResultUiModel
import com.filippetrovic.polygloteye.ui.objectdetection.model.DetectionUiModel
import com.filippetrovic.polygloteye.ui.objectdetection.model.ObjectDetectionViewState
import com.filippetrovic.polygloteye.usecase.TextToSpeechUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ObjectDetectionViewModel @Inject constructor(
    private val objectDetectorManager: IObjectDetectorManager,
    private val translationDataSource: ITranslationDataSource,
    private val userPreferences: IUserPreferencesRepository,
    private val dictionaryRepository: IDictionaryRepository,
    private val textToSpeechUseCase: TextToSpeechUseCase
) : ViewModel() {

    // region Companion

    companion object {
        const val GET_LANGUAGES_REPEAT_INTERVAL_ON_ERROR = 5000L
    }

    // endregion

    // region Properties

    private val _state = MutableStateFlow(ObjectDetectionViewState())
    val state: StateFlow<ObjectDetectionViewState>
        get() = _state

    // endregion

    fun initializeObjectDetector() {
        objectDetectorManager.initialize()
            .onError(::handleError)
    }

    fun getSupportedLanguages() {
        viewModelScope.launch {
            translationDataSource.getSupportedLanguages()
                .onResult(::handleGetSupportedLanguagesResult)
                .onErrorSuspend(::handleGetSupportedLanguagesError)
        }
    }

    fun getLastUsedLanguages() {
        viewModelScope.launch {
            val sourceLanguage = userPreferences.getSourceLanguage().firstOrNull()
                ?: LanguageModel.getDefaultLanguageModel()
            val targetLanguage = userPreferences.getTargetLanguage().firstOrNull()
                ?: LanguageModel.getDefaultLanguageModel()

            _state.update {
                it.copy(
                    sourceLanguage = sourceLanguage,
                    targetLanguage = targetLanguage
                )
            }
        }
    }

    fun detectObjects(image: ImageProxy) {
        val bitmap = Bitmap.createBitmap(
            image.width,
            image.height,
            Bitmap.Config.ARGB_8888
        )

        image.use { bitmap.copyPixelsFromBuffer(image.planes[0].buffer) }

        val imageRotation = image.imageInfo.rotationDegrees

        objectDetectorManager.detect(bitmap, imageRotation)
            .onResult(::handleDetectionResult)
            .onError(::handleError)

        image.close()
    }

    fun setSourceLanguage(languageModel: LanguageModel) {
        viewModelScope.launch {
            userPreferences.updateSourceLanguage(languageModel)
        }

        _state.update { it.copy(sourceLanguage = languageModel) }
    }

    fun setTargetLanguage(languageModel: LanguageModel) {
        viewModelScope.launch {
            userPreferences.updateTargetLanguage(languageModel)
        }

        _state.update { it.copy(targetLanguage = languageModel) }
    }

    fun swapLanguages() {
        val newTargetLanguage = _state.value.sourceLanguage
        val newSourceLanguage = _state.value.targetLanguage

        viewModelScope.launch {
            userPreferences.updateSourceLanguage(newSourceLanguage)
            userPreferences.updateTargetLanguage(newTargetLanguage)
        }

        _state.update {
            it.copy(
                sourceLanguage = newSourceLanguage,
                targetLanguage = newTargetLanguage
            )
        }
    }

    fun synthesize(detection: DetectionUiModel) {
        if (detection.targetLanguageLabel.isEmpty()) return

        viewModelScope.launch {
            textToSpeechUseCase(detection.targetLanguageLabel, _state.value.targetLanguage.code)
                .onError(::handleError)
        }
    }

    fun clearDetections() {
        _state.update { it.copy(detectionResult = DetectionResultUiModel()) }
    }

    private fun handleDetectionResult(result: DetectionResult) {
        _state.update {
            it.copy(
                detectionResult = DetectionResultUiModel.mapToDetectionResultUiModel(
                    result = result,
                    sourceLanguage = it.sourceLanguage,
                    targetLanguage = it.targetLanguage,
                    dictionaryRepository = dictionaryRepository
                )
            )
        }
    }

    private fun handleError(exception: Exception) {
        _state.update {
            it.copy(
                errorMessage = Consumable(StringHolder(exception.message ?: ""))
            )
        }
    }

    private fun handleGetSupportedLanguagesResult(supportedLanguages: List<LanguageModel>) {
        _state.update { it.copy(supportedLanguages = supportedLanguages) }
    }

    private suspend fun handleGetSupportedLanguagesError(exception: Exception) {
        _state.update { it.copy(errorMessage = Consumable(StringHolder(R.string.connection_error))) }

        delay(GET_LANGUAGES_REPEAT_INTERVAL_ON_ERROR)
        getSupportedLanguages()
    }
}
