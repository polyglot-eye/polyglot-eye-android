package com.filippetrovic.polygloteye.ui.objectdetection.model

import com.filippetrovic.polygloteye.data.datasource.translation.model.LanguageModel
import com.filippetrovic.polygloteye.functional.Consumable
import com.filippetrovic.polygloteye.functional.StringHolder

data class ObjectDetectionViewState(
    val detectionResult: DetectionResultUiModel = DetectionResultUiModel(),
    val errorMessage: Consumable<StringHolder>? = null,
    val sourceLanguage: LanguageModel = LanguageModel.getDefaultLanguageModel(),
    val targetLanguage: LanguageModel = LanguageModel.getDefaultLanguageModel(),
    val supportedLanguages: List<LanguageModel> = emptyList()
)
