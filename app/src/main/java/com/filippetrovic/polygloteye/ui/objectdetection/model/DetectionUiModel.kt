package com.filippetrovic.polygloteye.ui.objectdetection.model

import android.graphics.RectF
import com.filippetrovic.polygloteye.data.datasource.translation.model.LanguageModel
import com.filippetrovic.polygloteye.data.repository.dictionary.IDictionaryRepository
import org.tensorflow.lite.task.vision.detector.Detection

data class DetectionUiModel(
    val boundingBox: RectF,
    val sourceLanguageLabel: String,
    val targetLanguageLabel: String
) {

    companion object {

        fun mapToDetectionUiModelList(
            detections: List<Detection>,
            sourceLanguage: LanguageModel,
            targetLanguage: LanguageModel,
            dictionaryRepository: IDictionaryRepository
        ): List<DetectionUiModel> {
            return detections.map {
                mapToDetectionUiModel(it, sourceLanguage, targetLanguage, dictionaryRepository)
            }
        }

        private fun mapToDetectionUiModel(
            detection: Detection,
            sourceLanguage: LanguageModel,
            targetLanguage: LanguageModel,
            dictionaryRepository: IDictionaryRepository
        ): DetectionUiModel {
            val label = detection.categories.first().label

            val sourceLanguageLabel =
                dictionaryRepository.getTranslation(label, sourceLanguage.code).orEmpty()

            val targetLanguageLabel =
                dictionaryRepository.getTranslation(label, targetLanguage.code).orEmpty()

            return DetectionUiModel(detection.boundingBox, sourceLanguageLabel, targetLanguageLabel)
        }
    }
}
