package com.filippetrovic.polygloteye.ui.objectdetection.model

import com.filippetrovic.polygloteye.data.datasource.translation.model.LanguageModel
import com.filippetrovic.polygloteye.data.manager.detection.DetectionResult
import com.filippetrovic.polygloteye.data.repository.dictionary.IDictionaryRepository

data class DetectionResultUiModel(
    val detections: List<DetectionUiModel> = emptyList(),
    val inferenceTime: Long = 0L,
    val imageHeight: Int = 0,
    val imageWidth: Int = 0
) {

    companion object {

        fun mapToDetectionResultUiModel(
            result: DetectionResult,
            sourceLanguage: LanguageModel,
            targetLanguage: LanguageModel,
            dictionaryRepository: IDictionaryRepository
        ): DetectionResultUiModel {
            return DetectionResultUiModel(
                DetectionUiModel.mapToDetectionUiModelList(
                    result.detections,
                    sourceLanguage,
                    targetLanguage,
                    dictionaryRepository
                ),
                result.inferenceTime,
                result.imageHeight,
                result.imageWidth
            )
        }
    }
}
