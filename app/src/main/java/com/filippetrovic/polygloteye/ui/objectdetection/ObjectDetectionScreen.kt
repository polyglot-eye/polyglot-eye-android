package com.filippetrovic.polygloteye.ui.objectdetection

import android.graphics.RectF
import android.view.Surface.ROTATION_0
import android.view.ViewGroup.LayoutParams
import android.widget.LinearLayout
import android.widget.Toast
import androidx.camera.core.AspectRatio
import androidx.camera.core.CameraSelector
import androidx.camera.core.CameraState
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.LifecycleCameraController
import androidx.camera.view.PreviewView
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.SwapHoriz
import androidx.compose.material.icons.filled.VolumeUp
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.platform.LocalTextInputService
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextMeasurer
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.drawText
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.rememberTextMeasurer
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.filippetrovic.polygloteye.data.datasource.translation.model.LanguageModel
import com.filippetrovic.polygloteye.extensions.area
import com.filippetrovic.polygloteye.ui.objectdetection.model.DetectionResultUiModel
import com.filippetrovic.polygloteye.ui.objectdetection.model.DetectionUiModel
import com.filippetrovic.polygloteye.ui.objectdetection.model.ObjectDetectionViewState
import com.filippetrovic.polygloteye.ui.theme.DetectionColor
import com.filippetrovic.polygloteye.viewmodel.ObjectDetectionViewModel
import com.google.common.util.concurrent.ListenableFuture
import java.util.concurrent.Executor
import kotlin.math.max

@Composable
fun ObjectDetectionScreen(
    objectDetectionViewModel: ObjectDetectionViewModel = hiltViewModel()
) {
    LaunchedEffect(Unit) {
        objectDetectionViewModel.initializeObjectDetector()
        objectDetectionViewModel.getSupportedLanguages()
        objectDetectionViewModel.getLastUsedLanguages()
    }

    val state by objectDetectionViewModel.state.collectAsStateWithLifecycle()

    Surface(color = Color.Black) {
        ObjectDetectionContent(
            state,
            objectDetectionViewModel::detectObjects,
            objectDetectionViewModel::setSourceLanguage,
            objectDetectionViewModel::setTargetLanguage,
            objectDetectionViewModel::swapLanguages,
            objectDetectionViewModel::synthesize,
            objectDetectionViewModel::clearDetections
        )
    }

    KeepScreenOn()
}

@Composable
private fun ObjectDetectionContent(
    state: ObjectDetectionViewState,
    detectObjects: (image: ImageProxy) -> Unit,
    onSourceLanguageChange: (LanguageModel) -> Unit,
    onTargetLanguageChange: (LanguageModel) -> Unit,
    onSwapLanguages: () -> Unit,
    synthesize: (DetectionUiModel) -> Unit,
    clearDetections: () -> Unit
) {
    var sourceLanguageExpanded by remember { mutableStateOf(false) }
    var targetLanguageExpanded by remember { mutableStateOf(false) }

    val localContext = LocalContext.current

    CameraView(
        state.detectionResult,
        sourceLanguageExpanded,
        targetLanguageExpanded,
        detectObjects,
        synthesize,
        clearDetections
    )

    state.errorMessage?.consume()?.let {
        Toast.makeText(localContext, it.getString(LocalContext.current), Toast.LENGTH_LONG).show()
    }

    if (state.supportedLanguages.isNotEmpty()) {
        LanguageChooser(
            state,
            sourceLanguageExpanded,
            targetLanguageExpanded,
            { sourceLanguageExpanded = it },
            { targetLanguageExpanded = it },
            onSourceLanguageChange,
            onTargetLanguageChange,
            onSwapLanguages
        )
    }
}

@Composable
private fun CameraView(
    detectionResult: DetectionResultUiModel,
    sourceLanguageExpanded: Boolean,
    targetLanguageExpanded: Boolean,
    detectObjects: (image: ImageProxy) -> Unit,
    synthesize: (DetectionUiModel) -> Unit,
    clearDetections: () -> Unit
) {
    val localContext = LocalContext.current
    val lifecycleOwner = LocalLifecycleOwner.current
    val cameraProviderFuture = remember { ProcessCameraProvider.getInstance(localContext) }
    val cameraController: LifecycleCameraController = remember { LifecycleCameraController(localContext) }
    var cameraState by remember { mutableStateOf(CameraState.Type.OPENING) }

    if (!sourceLanguageExpanded && !targetLanguageExpanded) {
        AndroidView(
            factory = { context ->
                val previewView = PreviewView(context).apply {
                    layoutParams = LinearLayout.LayoutParams(
                        LayoutParams.MATCH_PARENT,
                        LayoutParams.MATCH_PARENT
                    )

                    setBackgroundColor(android.graphics.Color.BLACK)
                    scaleType = PreviewView.ScaleType.FILL_START
                }

                val executor = ContextCompat.getMainExecutor(context)

                setCameraProviderListener(
                    lifecycleOwner,
                    cameraProviderFuture,
                    previewView,
                    executor,
                    detectObjects
                ) { cameraState = it }

                previewView.controller = cameraController
                cameraController.bindToLifecycle(lifecycleOwner)

                previewView
            }
        )

        DetectionsView(detectionResult, synthesize)

        if (cameraState != CameraState.Type.OPEN) {
            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                CircularProgressIndicator()
            }
        }
    } else {
        clearDetections()
    }
}

@Composable
private fun DetectionsView(
    detectionResult: DetectionResultUiModel,
    synthesize: (DetectionUiModel) -> Unit
) {
    val density = LocalDensity.current
    val configuration = LocalConfiguration.current
    val textMeasurer = rememberTextMeasurer()
    val detections by rememberUpdatedState(newValue = detectionResult.detections)

    val screenHeight = configuration.screenHeightDp.dp
    val screenWidth = configuration.screenWidthDp.dp

    val scaleFactor by rememberUpdatedState(
        newValue = max(
            screenWidth.value / detectionResult.imageWidth,
            screenHeight.value / detectionResult.imageHeight
        )
    )

    val volumePainter = rememberVectorPainter(Icons.Default.VolumeUp)

    Canvas(
        modifier = Modifier
            .clipToBounds()
            .pointerInput(Unit) {
                detectTapGestures { tapOffset ->
                    handleCanvasTapGesture(tapOffset, detections, scaleFactor, density, synthesize)
                }
            }
    ) {
        detectionResult.detections.forEach { detection ->

            val detectionRect = getScaledRectF(density, detection.boundingBox, scaleFactor)

            detectionRect.apply {
                drawRect(
                    color = DetectionColor,
                    size = Size(width = right - left, height = bottom - top),
                    topLeft = Offset(x = left, y = top),
                    style = Stroke(width = 2.dp.toPx())
                )
            }

            val annotatedText = createDetectedObjectTranslation(
                sourceLanguageLabel = detection.sourceLanguageLabel,
                targetLanguageLabel = detection.targetLanguageLabel
            )

            drawText(
                textMeasurer = textMeasurer,
                text = annotatedText,
                topLeft = Offset(
                    detectionRect.left.coerceIn(0f, screenWidth.toPx()),
                    detectionRect.top.coerceIn(0f, screenHeight.toPx())
                ),
                softWrap = false
            )

            translate(
                left = getVolumePainterLeftTranslation(detectionRect, textMeasurer, detection.targetLanguageLabel, density),
                top = detectionRect.top.coerceIn(0f, screenHeight.toPx())
            ) {
                with(volumePainter) {
                    draw(
                        volumePainter.intrinsicSize,
                        colorFilter = ColorFilter.tint(DetectionColor)
                    )
                }
            }
        }
    }
}

@Composable
fun LanguageChooser(
    state: ObjectDetectionViewState,
    sourceLanguageExpanded: Boolean,
    targetLanguageExpanded: Boolean,
    onSourceLanguageExpandToggle: (Boolean) -> Unit,
    onTargetLanguageExpandToggle: (Boolean) -> Unit,
    onSourceLanguageChange: (LanguageModel) -> Unit,
    onTargetLanguageChange: (LanguageModel) -> Unit,
    onSwapLanguages: () -> Unit
) {
    Box(
        contentAlignment = Alignment.BottomCenter,
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Column(Modifier.weight(1f)) {
                LanguageDropdownMenuBox(
                    sourceLanguageExpanded,
                    onSourceLanguageExpandToggle,
                    state.supportedLanguages,
                    state.sourceLanguage,
                    onSourceLanguageChange
                )
            }

            Column(Modifier.padding(horizontal = 16.dp)) {
                IconButton(onClick = onSwapLanguages) {
                    Icon(
                        imageVector = Icons.Default.SwapHoriz,
                        contentDescription = "Swap languages",
                        tint = DetectionColor
                    )
                }
            }
            Column(Modifier.weight(1f)) {
                LanguageDropdownMenuBox(
                    targetLanguageExpanded,
                    onTargetLanguageExpandToggle,
                    state.supportedLanguages,
                    state.targetLanguage,
                    onTargetLanguageChange
                )
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LanguageDropdownMenuBox(
    expanded: Boolean,
    onExpandToggle: (Boolean) -> Unit,
    supportedLanguages: List<LanguageModel>,
    language: LanguageModel,
    onLanguageChange: (LanguageModel) -> Unit
) {
    ExposedDropdownMenuBox(
        expanded = expanded,
        onExpandedChange = onExpandToggle
    ) {
        CompositionLocalProvider(
            LocalTextInputService provides null
        ) {
            TextField(
                value = language.name,
                onValueChange = {},
                readOnly = true,
                textStyle = LocalTextStyle.current.copy(textAlign = TextAlign.Center),
                singleLine = true,
                colors = TextFieldDefaults.textFieldColors(
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                    disabledIndicatorColor = Color.Transparent
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .menuAnchor(),
                shape = MaterialTheme.shapes.extraLarge
            )
        }

        ExposedDropdownMenu(
            expanded = expanded,
            onDismissRequest = { onExpandToggle(false) }
        ) {
            supportedLanguages.forEach { item ->
                DropdownMenuItem(
                    text = { Text(text = item.name) },
                    onClick = {
                        onLanguageChange(item)
                        onExpandToggle(false)
                    }
                )
            }
        }
    }
}

@Composable
fun KeepScreenOn() {
    val currentView = LocalView.current
    DisposableEffect(Unit) {
        currentView.keepScreenOn = true
        onDispose {
            currentView.keepScreenOn = false
        }
    }
}

private fun setCameraProviderListener(
    lifecycleOwner: LifecycleOwner,
    cameraProviderFuture: ListenableFuture<ProcessCameraProvider>,
    previewView: PreviewView,
    executor: Executor,
    detectObjects: (image: ImageProxy) -> Unit,
    onCameraStateChange: (CameraState.Type) -> Unit
) {
    cameraProviderFuture.addListener({
        val cameraProvider = cameraProviderFuture.get()
        val preview = androidx.camera.core.Preview.Builder()
            .setTargetAspectRatio(AspectRatio.RATIO_4_3)
            .setTargetRotation(previewView.display?.rotation ?: ROTATION_0)
            .build().also {
                it.setSurfaceProvider(previewView.surfaceProvider)
            }

        val imageAnalyzer =
            ImageAnalysis.Builder()
                .setTargetAspectRatio(AspectRatio.RATIO_4_3)
                .setTargetRotation(preview.targetRotation)
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .setOutputImageFormat(ImageAnalysis.OUTPUT_IMAGE_FORMAT_RGBA_8888)
                .build()
                .also {
                    it.setAnalyzer(executor) { image ->
                        detectObjects(image)
                    }
                }

        val cameraSelector = CameraSelector.Builder()
            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
            .build()

        cameraProvider.unbindAll()
        cameraProvider.bindToLifecycle(
            lifecycleOwner,
            cameraSelector,
            preview,
            imageAnalyzer
        )

        val camera = cameraProvider.bindToLifecycle(lifecycleOwner, cameraSelector)
        camera.cameraInfo.cameraState.removeObservers(lifecycleOwner)
        camera.cameraInfo.cameraState.observe(lifecycleOwner) {
            onCameraStateChange(it.type)
        }
    }, executor)
}

private fun createDetectedObjectTranslation(
    sourceLanguageLabel: String,
    targetLanguageLabel: String
): AnnotatedString {
    return buildAnnotatedString {
        withStyle(
            style = SpanStyle(
                fontSize = 18.sp,
                color = Color.Black,
                fontWeight = FontWeight.ExtraBold,
                background = DetectionColor
            )
        ) {
            append(" $targetLanguageLabel \n")
        }
        withStyle(
            style = SpanStyle(
                fontSize = 18.sp,
                color = Color.Black,
                fontWeight = FontWeight.Medium,
                background = DetectionColor
            )
        ) {
            append(" $sourceLanguageLabel ")
        }
    }
}

private fun getScaledRectF(density: Density, rectangle: RectF, scaleFactor: Float) = run {
    with(density) {
        val leftEdge = (rectangle.left.dp * scaleFactor).toPx()
        val topEdge = (rectangle.top.dp * scaleFactor).toPx()
        val rightEdge = (rectangle.right.dp * scaleFactor).toPx()
        val bottomEdge = (rectangle.bottom.dp * scaleFactor).toPx()

        return@with RectF(leftEdge, topEdge, rightEdge, bottomEdge)
    }
}

private fun handleCanvasTapGesture(
    tapOffset: Offset,
    detections: List<DetectionUiModel>,
    scaleFactor: Float,
    density: Density,
    synthesize: (DetectionUiModel) -> Unit
) {
    val tappedDetections = detections.filter {
        val detectionRect = getScaledRectF(density, it.boundingBox, scaleFactor)
        detectionRect.contains(tapOffset.x, tapOffset.y)
    }

    tappedDetections
        .minByOrNull { it.boundingBox.area() }
        ?.let(synthesize)
}

private fun getVolumePainterLeftTranslation(
    detectionRect: RectF,
    textMeasurer: TextMeasurer,
    text: String,
    density: Density
): Float {
    with(density) {
        val leftEdge = detectionRect.left.coerceAtLeast(0f)
        val textWidth = textMeasurer.measure(
            " $text ",
            TextStyle.Default.copy(
                fontSize = 18.sp,
                fontWeight = FontWeight.ExtraBold
            )
        ).size.width

        return leftEdge + textWidth + 4.dp.toPx()
    }
}
