package com.filippetrovic.polygloteye.functional

import android.content.Context
import androidx.annotation.StringRes

class StringHolder() {

    private var value: Any = ""
    private var args: Array<out Any> = emptyArray()

    constructor(string: String) : this() {
        value = string
        args = emptyArray()
    }

    constructor(@StringRes stringRes: Int, vararg formatArgs: Any) : this() {
        value = stringRes
        args = formatArgs
    }

    override fun equals(other: Any?): Boolean {
        return (other as? StringHolder)?.getString() == getString()
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }

    fun getString(context: Context? = null): String {
        return (value as? Int)?.let {
            val finalArgs = Array(args.size) { index ->
                (args[index] as? StringHolder)?.getString(context) ?: args[index]
            }

            context?.getString(it, *finalArgs).orEmpty()
        } ?: value as String
    }
}
