package com.filippetrovic.polygloteye.functional

data class Consumable<T>(private var value: T? = null) {

    fun consume(): T? {
        val toConsume = value
        value = null
        return toConsume
    }
}
