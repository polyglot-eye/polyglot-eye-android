# Polyglot Eye

## Introduction

This is the second part of practical demonstration for the Master thesis: "Application of neural networks in android apps"
This project represents the android application which uses the neural network for object detection that was trained in the first part of practical demonstration.

The application uses camera to gather frames which are processed using neural network model in the application. When objects are detected, user can see the names of the object, and can also see the translation of those objects.

[Download from play store](https://play.google.com/store/apps/details?id=com.filippetrovic.polygloteye)

## Technologies and tools

- Kotlin
- Model-View-ViewModel (MVVM) architectural pattern
- Clean Architecture
- Hilt
- Coroutines
- Flows
- DataStore
- Jetpack Compose
- Mockk for unit testing
- TensorFlow Lite
- Camera API
- Cloud Translation API
- Cloud Text-to-Speech API
- Version Catalog

## Setup

To run the app, you will need to generate credentials in the Google Cloud Console for the following APIs:
- Cloud Translation API
- Cloud Text-to-Speech API

You will also need a .tflite model that you can train using the [Trainer project](https://gitlab.com/polyglot-eye/polyglot-eye-trainer), or download it from the internet

## UI

<div align="center">
  <img src="./screenshots/es - bicyle.jpg" width="27%"> &nbsp; &nbsp; &nbsp; &nbsp;
  <img src="./screenshots/de - stop sign and car.jpg" width="27%" style="margin-left:20px"> &nbsp; &nbsp; &nbsp; &nbsp;
  <img src="./screenshots/fr - book and cup.jpg" width="27%">
</div>

&nbsp;

<div align="center">
  <img src="./screenshots/sr - bench.jpg" width="21%"> &nbsp;
  <img src="./screenshots/es - potted plant.jpg" width="21%"> &nbsp;
  <img src="./screenshots/ja - person.jpg" width="21%"> &nbsp;
  <img src="./screenshots/ru - cat.jpg" width="21%">
</div>
